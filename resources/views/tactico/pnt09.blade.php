@extends('layouts.template')
@section('content')
<div id=contact>
  <div class="container-fluid">
    <div class="section-header">
      <h4 align="center"><b>Reporte de Prorrogas turísticas de niños y adolescentes de hijos de padres salvadoreños con pasaporte extranjero y de padres extranjeros.</b>  </h4>
    </div>
    <div class="col-lg-12" align="center">
       @if($vacio ==null)
        <div class="col-lg-12">
          <h5>Seleccione la Fecha Inicio y Fecha Final</h5>
        </div>
        @endif
        <div class="form" style="align-content: center;">
              <form action="{{route('prorroga.PTNAHPSyPTNAHPE')}}" method="post" role="form" class="contactForm">
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="col-lg-3">
                    @if($vacio ==null)
                    <label class="form-control" style="border:0px;"> Fecha Inicio: </label>
                    </div>
                    <div class="form-group col-lg-3">
                    <input type="date" name="fecha_inicio" class="form-control" id="name" required="required" />
                    <div class="validation"></div>
                    </div>
                    <div class="col-lg-3">
                    <label class="form-control" style="border:0px;"> Fecha Final: </label>
                    </div>
                    <div class="form-group col-lg-3">
                    <input type="date" name="fecha_final" class="form-control" id="name" required="required" />
                    <div class="validation"></div>
                    </div>
                 </div>                 
                <div class="col-lg-12" style="padding-top: 3%; padding-bottom: 2%">
                    <button class="btn-primary" >Consultar</button>
                </div>
                @endif
              </form>
          </div>
          <div class="col-md-12">
             <div>
                    @if($vacio !=null)
                    <h5 align="center" > <b>Total de Prorrogas a Hijos de Salvadoreños e Hijos de Extranjeros </b>
                      <h5 class="col-md-6" style="float: left;">Desde : {{date('j', strtotime($finicio))}} de {{ $ife }} del {{date('Y', strtotime($finicio))}}</h5>
                    <h5 class="col-md-6" style="float: right;"> Hasta: {{date('j', strtotime($ffinal))}} de {{ $fe }} del {{date('Y', strtotime($ffinal))}}</h5>
                        
                    </h5>
                    @endif
              </div>
            </div>
                  <div class="col-md-6" style="float: left">
                    <table class="table table-striped table-hover" id="reporte" style="background: #fff;">
                            <thead>
                              @if($vacio !=null)
                              <tr>
                                <th style="text-align: center">Edad</th>
                                <th style="text-align: center">Hijos de Salvadoreños</th>
                              </tr>
                            @endif
                            </thead>
                            <tbody>
                               @if($vacio != null)
                            <tr>
                               @foreach($hijosSalvador as $hijosSalvador)
                               
                                <td align="center">{{$hijosSalvador->edad}}</td>
                                <td align="center">{{$hijosSalvador->hijo}}</td>
                              </tr>
                            @endforeach
                             @else
                             <tr>
                               <td></td>
                               <td></td>
                             </tr>
                           @endif
                            </tbody>
                            <tfoot>
                              @if($vacio != null)
                    <tr>
                        <td style="font-size: 1.5em; text-align: center"><b>Totales</b></td>
                        <td style="font-size: 1.5em" align="center"><b>{{$totalH}}</b></td>
                    </tr>
                    @endif
                            </tfoot>
                        </table>
                  </div>
                        
                     <div class="col-md-6" style="float: right;">
                        <table class="table table-striped table-hover" id="reporte" style="background: #fff">
                          
                            <thead>
                              @if($vacio !=null)
                              <tr>
                                <th style="text-align: center">Edad</th>
                                <th style="text-align: center">Hijos de Extrajeros</th>
                              </tr>
                            @endif
                            </thead>
                            <tbody>
                               @if($vacio != null)
                              <tr>
                              @foreach($extranjero as $extranjero)
                                <td align="center">{{$extranjero->edad}}</td>
                                <td align="center">{{$extranjero->extranjero}}</td>
                                
                            </tr>
                            @endforeach
                             @else
                             <tr>
                               <td></td>
                               <td></td>
                             </tr>
                           @endif
                            </tbody>
                            <tfoot>
                              @if($vacio != null)
                    <tr>
                        <td style="font-size: 1.5em;text-align: center" ><b>Totales</b></td>
                        <td style="font-size: 1.5em" align="center"><b>{{$totalE}}</b></td>
                    </tr>
                    @endif
                            </tfoot>
                        </table>
                      </div>
                  <div class="col-md-12">

            <div class="form" style="align-content: center;">
            <form action="{{route('prorroga.pdfprorrogaPTNAHPSyPTNAHPE')}}" method="post" role="form" class="contactForm" target="_blank">
                 {{ csrf_field() }}
                <div class="col-lg-12">
                  @if($vacio != null)
                  <input id="fechaini" type="hidden" class="form-control" value="{{$finicio}}" name="fechaini">
                   <input id="fechafin" type="hidden" class="form-control" value="{{$ffinal}}" name="fechafin">
                    <button class="btn-primary">Generar PDF</button> 
                    <a class="btn btn-secondary" style="background: #dddddd; color: black" href="{{ route('prorroga.verPTNAHPSyPTNAHPE') }}">Resetear</a>
                </div>
                  @endif
              </form>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection
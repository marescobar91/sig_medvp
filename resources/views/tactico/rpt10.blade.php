<!DOCTYPE html>

<head>
<title>SIG-MEDVP</title>
 <link href="css/style.css" rel="stylesheet">
</head>
<body>
    <div id="wrapper">
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 30px">

                    <div class="col-md-9 col-md-offset-1">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">

                            <div class="row">
                                <div class="col-sm-2" style="margin: 8px 0px 3px 5px">
                                    <img src="{{public_path().'/img/Migracionlogo.jpg'}}" width="100px" align="left">
                                </div>
                                <div class="col-sm-7" style="text-align: center;" align="center">

                                    <h3 style="color: #1a1a1a;text-align: center;"><b>Dirección General de Migracion y Extranjeria</b></h3>
                                    <h4 style="color: #1a1a1a;text-align: center; padding-top: -20px"><b>Departamento de Visas y Prorrogas </b></h4>
                                    <h4 style="color: #1a1a1a;text-align: center; padding-top: 10px;padding-bottom: 20px"><b>Sistema de información gerencial para el manejo de estadísticas del departamento de visas y prórrogas para la dirección general de migración y extranjería.</b></h4>
                                </div>

                            </div>
                            <hr style="display: block;margin-top: -5px;margin-bottom: 0.5em;margin-left: 5px;margin-right: auto;border-style: inset;border-width: 0.75px; width: 95%; border-color: #1a1a1a"/>
                            <br/>

                            <div class="row">
                                
                    <div style="max-height: 700px;">
             <div>
                    <h3 align="center" > <b>Reporte de Visas Multiples no Residentes </b></h3>
                    <h4 style="margin-left: 120px"> <b>Desde  : </b> {{date('j', strtotime($fecha_inicio))}} de {{ $ife }} del {{date('Y', strtotime($fecha_inicio))}}</h4>
                    <h4 style="margin-left: 120px"> <b> Hasta : </b> {{date('j', strtotime($fecha_final))}} de {{ $fe }} del {{date('Y', strtotime($fecha_final))}} 
                        
                    </h4>
              </div>
              </div>

               <table class="table table-striped table-hover" id="reporte" style="background: #fff;width: 75%;margin-left: 90px; padding-top: 30px" >
                <thead>
                  <tr>
                    <th width="60%">Pais</th>
                    <th width="40%">Total</th>
                </tr>
                </thead>
                 <tbody>
                    @foreach($visasM as $visas)
                <tr>
                   
                    <td>{{ $visas->nombre}}</td>
                    <td>{{ $visas->total_visa_multiple }}</td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                
                    <tr>
                        <td style="font-size: 1.5em"><b>Totales</b></td>
                        <td style="font-size: 1.5em"><b>{{$total}}</b></td>
                    </tr>
                </tfoot>
                
            </table>

<div style="float: right; padding-top: 1%;"><label> {{ \Carbon\Carbon::now(new DateTimeZone('America/El_Salvador'))->format('d/m/Y H:i:s')}}</label></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

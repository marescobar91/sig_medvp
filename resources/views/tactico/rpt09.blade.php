<!DOCTYPE html>

<head>
<title>SIG-MEDVP</title>
 <link href="css/style.css" rel="stylesheet">
 <style type="text/css">
     .table-strip tbody tr:nth-of-type(odd){
        background-color: rgba(0, 0, 0, 0.05);
     }
     .table-strip tbody tr:nth-of-type(odd) {
  background-color: rgba(255, 255, 255, 0.05);
}
.table th,
.table td {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #fff;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #fff;
}

.table tbody + tbody {
  border-top: 2px solid #fff;
}
.table-bordered {
  border: 1px solid #fff;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #fff;
}
.table .thead-light th {
  color: #495057;
  background-color: #e9ecef;
  border-color: #fff;
}

 </style>
</head>
<body>
    <div id="wrapper"> 
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="container" style="margin-left: 30px">

                    <div class="col-md-9 col-md-offset-1">
                        <div class="table-title" style="background-color: #fff; margin-bottom: 1%">

                            <div class="row">
                                <div class="col-sm-2" style="margin: 8px 0px 3px 5px">
                                    <img src="{{public_path().'/img/Migracionlogo.jpg'}}" width="100px" align="left">
                                </div>
                                <div class="col-sm-7" style="text-align: center;" align="center">

                                    <h3 style="color: #1a1a1a;text-align: center;"><b>Dirección General de Migracion y Extranjeria</b></h3>
                                    <h4 style="color: #1a1a1a;text-align: center; padding-top: -20px"><b>Departamento de Visas y Prorrogas </b></h4>
                                    <h4 style="color: #1a1a1a;text-align: center; padding-top: 10px;padding-bottom: 20px"><b>Sistema de información gerencial para el manejo de estadísticas del departamento de visas y prórrogas para la dirección general de migración y extranjería.</b></h4>
                                </div>

                            </div>
                            <hr style="display: block;margin-top: -5px;margin-bottom: 0.5em;margin-left: 5px;margin-right: auto;border-style: inset;border-width: 0.75px; width: 95%; border-color: #1a1a1a"/>
                            <br/>

                            <div class="row">
                                
            <div style="max-height: 700px;">
             <div>
                    <h3 align="center">Reporte de Prorrogas turísticas de niños y adolescentes de hijos de padres salvadoreños con pasaporte extranjero y de padres extranjeros. </h3>
                    <h4 style="margin-left: 120px"> <b>Desde  : </b> {{date('j', strtotime($fecha_inicio))}} de {{ $ife }} del {{date('Y', strtotime($fecha_inicio))}}</h4>
                    <h4 style="margin-left: 120px"> <b> Hasta : </b> {{date('j', strtotime($fecha_final))}} de {{ $fe }} del {{date('Y', strtotime($fecha_final))}} 
                        
                    </h4>
              </div>
            </div>

              <div style="width: 700px; padding-top: 10px">
               
            <div style="float: left; width: 300px">
                    <table class="table table-striped table-hover" id="reporte" style="background: #fff; border:0px">
                            <thead>
                              
                              <tr>
                                <th style="text-align: center">Edad</th>
                                <th style="text-align: center">Hijos de Salvadoreños</th>
                              </tr>
                          
                            </thead>
                            <tbody>
                             @foreach($hijosSalvador as $hijosSalvador)  
                            <tr>
                               
                               
                                <td align="center">{{$hijosSalvador->edad}}</td>
                                <td align="center">{{$hijosSalvador->hijo}}</td>
                              </tr>
                            @endforeach
                           
                          
                            </tbody>
                            <tfoot>
                            
                    <tr >
                        <td style="font-size: 1.5em; text-align: center"><b>Totales</b></td>
                        <td style="font-size: 1.5em" align="center"><b>{{$totalH}}</b></td>
                    </tr>
                   
                            </tfoot>
                        </table>
                  </div>
                  <div style="float: right; width: 300px; margin-right: 25px">
                        <table class="table table-striped table-hover" id="reporte" style="background: #fff">
                          
                            <thead>
                              
                              <tr>
                                <th style="text-align: center">Edad</th>
                                <th style="text-align: center">Hijos de Extrajeros</th>
                              </tr>
                      
                            </thead>
                            <tbody>
                               @foreach($extranjero as $extranjero) 
                              <tr>
                             
                                <td align="center">{{$extranjero->edad}}</td>
                                <td align="center">{{$extranjero->extranjero}}</td>
                                
                            </tr>
                            @endforeach
                             
                            </tbody>
                            <tfoot>
                            
                    <tr>
                        <td style="font-size: 1.5em;text-align: center" ><b>Totales</b></td>
                        <td style="font-size: 1.5em" align="center"><b>{{$totalE}}</b></td>
                    </tr>
            
                            </tfoot>
                        </table>
                      </div>
                    </div>
<div style="float: right; padding-top: 1%;"><label> {{ \Carbon\Carbon::now(new DateTimeZone('America/El_Salvador'))->format('d/m/Y H:i:s')}}</label></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>


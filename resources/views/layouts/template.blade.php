<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SIG-MEDVP</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{asset('css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
    Author: Sig-medvp
    License: Open-Source
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left" style="margin-right: 22%">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="{{ url('/') }}" class="scrollto"><img src="{{asset('img/Migracionlogo.jpg')}}" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-left d-none d-lg-block">
        <ul>
          <li class="active"><a href="{{ route('home') }}" >Home</a></li>
          @can('menu-estrategico')
          <li class="drop-down"><a href="{{ route('home') }}">Estrategico</a>
            <ul>
              <li><a href="{{ route('prorroga.ver') }}">Prorrogas Otorgadas</a></li>
              <li><a href="{{ route('visas.ver') }}">Visas Otorgadas</a></li>
              <li><a href="{{ route('prorroga.verprorroga') }}">Estadistica de Prorrogas</a></li>
              <li><a href="{{route('visas.vervisa')}}">Estadisticas de Visas</a></li>
              <li><a href="{{route('tramites.estadistica')}}">Tramites por Nacionalidad</a></li>
              <li class="drop-down"><a href="{{ route('home') }}">Táctico</a>
            <ul>
              <li><a href="{{route('prorroga.verPTNAHPSyPTNAHPE')}}">PTNAHPS y PTNAHPE</a></li>
              <li class="drop-down"><a href="#">Visas Multiples </a>
                <ul>
                  <li><a href="{{route('visas.multiple.ver')}}">no Residentes</a></li>
                  <li><a href="{{route('visas.multiple.inversionista.ver')}}">para Inversionista</a></li>
                </ul>
              </li>
              <li class="drop-down"><a href="#">Permisos Especiales</a>
                <ul>
                  <li><a href="{{route('permiso.verEspecialIngreso')}}">para Ingreso</a></li>
                  <li><a href="{{route('permisos.verInversionista')}}">para Ingreso Inversionista</a></li>
                  <li><a href="{{route('permisos.marinos')}}">para Marinos</a></li>
                </ul>
              </li>
            </ul>
          </li>
            </ul>
          </li>
          @endcan
           @can('menu-tactico')
          <li class="drop-down"><a href="{{ route('home') }}">Táctico</a>
            <ul>
              <li><a href="{{route('prorroga.verPTNAHPSyPTNAHPE')}}">PTNAHPS y PTNAHPE</a></li>
              <li class="drop-down"><a href="#">Visas Multiples </a>
                <ul>
                  <li><a href="{{route('visas.multiple.ver')}}">no Residentes</a></li>
                  <li><a href="{{route('visas.multiple.inversionista.ver')}}">para Inversionista</a></li>
                </ul>
              </li>
              <li class="drop-down"><a href="#">Permisos Especiales</a>
                <ul>
                  <li><a href="{{route('permiso.verEspecialIngreso')}}">para Ingreso</a></li>
                  <li><a href="{{route('permisos.verInversionista')}}">para Ingreso Inversionista</a></li>
                  <li><a href="{{route('permisos.marinos')}}">para Marinos</a></li>
                </ul>
              </li>
            </ul>
          </li>
          @endcan
           
           @can('menu-administrador')
          <li class="drop-down"><a href="{{ route('home') }}">Administrator </a>
            <ul>
              <li><a href="{{ route('user.index') }}">Gestion de usuarios</a></li>
              <li><a href="{{ route('user.seguridad') }}">Seguridad de Roles</a></li>
            </ul>
          </li>
          @endcan
         <li>
              <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('register') }}">Registrarse</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle btn-login" data-toggle="dropdown" role="button" aria-expanded="false" style="color: #fff">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                   <li><a href="{{route('user.password')}}">Cambiar Contraseña</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
          </li>
        </ul>
      </nav><!-- .main-nav -->
      <div style="float: right; padding-top: 1%;font-size: 12px"><label> {{ \Carbon\Carbon::now(new DateTimeZone('America/El_Salvador'))->format('d/m/Y H:i:s')}}</label></div>
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section  class="clearfix" style="background-color: #00438b;">
    <div class="container">
      <div class="intro-info" style="padding-top: 10%;  padding-bottom: 1%;">
        <h5 style="color: #fff; width: 95%;" align="center"><strong> Sistema de información gerencial para el <span>manejo de estadísticas del departamento de visas y prórrogas</span> para la dirección general de migración y extranjería.</strong><br></h5>
      </div>
    </div>
  </section><!-- #intro -->
<section class="clearfix">
  <div class="container">
    <br>
    @include('flash::message')
    @yield('content')
  </div>
</section>
  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
       <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>SIG-MEDVP</strong>. Derechos Reservados
      </div>
      <div class="credits">
        Diseñado por <a href="#">Equipo de SIG</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->

  <script src="{{asset('js/jquery.js')}}"></script>
  <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('lib/easing/easing.min.js')}}"></script>
  <script src="{{asset('lib/mobile-nav/mobile-nav.js')}}"></script>
  <script src="{{asset('lib/wow/wow.min.js')}}"></script>
  <script src="{{asset('lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{asset('lib/counterup/counterup.min.js')}}"></script>
  <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('lib/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('lib/lightbox/js/lightbox.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="{{asset('js/main.js')}}"></script>
  @yield('js')
</body>
</html>

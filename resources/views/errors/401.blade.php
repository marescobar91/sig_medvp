@extends('layouts.template')
@section('content')
		<div class="row">
          <div class="span12">
            <div class="centered">
              <h2 class="error">401</h2>
              <i class="icon-remove-circle icon-circled icon-128 active"></i>
              <h3>Lamentamos, esta pagina no esta disponible!</h3>
              <p>
              	La pagina que desea ingresar no tiene acceso o se ha eliminado temporalmente
              </p>
            </div>
          </div>
        </div>
@endsection

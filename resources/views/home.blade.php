@extends('layouts.template')
@section('content')

 @can('menu-estrategico')
<section> <!--Menu Estrategico-->
<div id="portfolio">
<header class="section-header">
          <h3 class="section-title">Menú Principal</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">Todos</li>
              <li data-filter=".filter-app">Estrategicos</li>
              <li data-filter=".filter-card">Tácticos</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app"> 
            <div class="portfolio-wrap">
              <img src="img/portfolio/app4.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{ route('prorroga.ver') }}" class="link-details" title="Mostrar"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app5.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('tramites.estadistica')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card10.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('prorroga.verPTNAHPSyPTNAHPE')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app6.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{ route('visas.ver') }}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card11.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  
                  <a href="{{route('visas.multiple.ver')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app7.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{ route('prorroga.verprorroga') }}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card12.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('visas.multiple.inversionista.ver')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/app8.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{ route('visas.vervisa') }}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card13.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('permiso.verEspecialIngreso')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card14.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('permisos.verInversionista')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card15.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('permisos.marinos')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
</section>
@endcan
 @can('menu-tactico')
<section> <!--Menu Tactico -->
    <div id="portfolio">
      <header class="section-header">
          <h3 class="section-title">Menú Principal</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li>Tácticos</li>
            <!--  <li data-filter=".filter-web">Web</li>-->
            </ul>
          </div>
        </div>
        <div class="row portfolio-container">          
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card10.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('prorroga.verPTNAHPSyPTNAHPE')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card11.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  
                  <a href="{{route('visas.multiple.ver')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card12.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('visas.multiple.inversionista.ver')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card13.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('permiso.verEspecialIngreso')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card14.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('permisos.verInversionista')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="img/portfolio/card15.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{route('permisos.marinos')}}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
</section>
@endcan

 @can('menu-administrador')
<section>  <!--Menu Administrator -->
  <div id="portfolio">
    <header class="section-header">
          <h3 class="section-title">Menú Principal</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">

            <ul id="portfolio-flters">
              <li>Administrador</li>
            <!--  <li data-filter=".filter-web">Web</li>-->
            </ul>
          </div>
        </div>
        <div class="row portfolio-container col-lg-12" style="align-content: center">     
        <div class="col-lg-1 col-md-6 portfolio-item filter-card">
          </div>     
          <div class="col-lg-5 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web1.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{ route('user.index') }}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-6  portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web2.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="{{ route('user.seguridad') }}" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    @endcan
@endsection
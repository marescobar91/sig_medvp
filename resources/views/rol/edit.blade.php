@extends('layouts.template')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-md-offset-1" align="center">
      <div class="panel panel-default">
        <div class="panel-heading"><h2 style="padding-top: 2%">Editar Roles</h2></div>
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="{{route('roles.update', $roles->id )}}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
                <div class="col-md-6">
                  <label for="name" class="col-md-4 control-label">Nombre</label>
                     <input id="nombre" type="text" class="form-control" name="nombre" required autofocus value="{{ $roles->name}}" placeholder="Nombre">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                  <label for="name" class="col-md-4 control-label">userName</label>
                     <input id="display_nombre" type="text" class="form-control" name="display_nombre" required autofocus value="{{ $roles->slug}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                  <label for="name" class="col-md-4 control-label">Descripción</label>
                     <textarea rows="4" cols="10" name="descripcion" id="descripcion" class="form-control" required>{{ $roles->description }}</textarea>     
                </div>
            </div>
            <div align="center">
                <h3>Permiso Especial</h3>
            </div>
            <div class="col-md-12 form-group">        
                      <select name="permiso_especial" class="span4 form-group">
                        <option @if(old('permiso_especial', $roles->special)== null) selected @endif>Selecciona Permiso Especial</option>
                        <option @if(old('permiso_especial', $roles->special)=='all-access') selected @endif>Acceso Total</option>
                        <option @if(old('permiso_especial', $roles->special)=='no-access') selected @endif>No acesso</option>
                      </select>
            </div>
            <div class="form-group" align="center">
                <h3>Asignar Privilegios</h3>
            </div>
            <div style="overflow: scroll; height: 300px;" >
               <div class="col-md-8 form-group" align="left">
                <ul>
                    @foreach($permisos as $permisos)
                      <label>
                        <input type="checkbox" value="{{$permisos->id}}"                        {{in_array($permisos->id, $permission) ? "checked" : null}} name="permissions[]">
                            {{$permisos->name}}</strong><em>({{$permisos->description ?: 'Sin descripción'}})</em>
                      </label>    
                    @endforeach
                </ul>
              </div>
            </div>
            <div class="text-center col-md-12 " style="padding-top: 3%;padding-bottom: 2%">
               <button class="btn btn-primary" type="submit">Guardar</button>
                  <a href="{{route('roles.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
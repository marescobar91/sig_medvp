@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-12 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-md-12">

                        <h1 align="center">Gestion de Usuarios</h1>
                        <div style="padding-left: 70%">
                        
                        <a href="{{ route('roles.create') }}" class="btn btn-primary"> <span>
                            <b>Crear Roles</b></span></a></div>
                             
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                   
                    <th>Rol</th>
                    <th> Slug</th>
                    <th>Descripcion </th>
                    
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($rol as $rol)
                <tr>
                    <td>{{ $rol->name }}</td>
                    <td>{{ $rol->slug}}</td>
                    <td>{{ $rol->description }}</td>
                   
                        <td>
                    
                        <a href="{{ route('roles.edit', $rol->id) }}" class="btn btn-warning"><span class="ion-edit" style="color: white"></span> </a>
                    
                    
                        <a class="btn btn-danger" onclick="eliminar('{{ $rol->id }}')"><span class="ion-trash-a" style="color: white"></span>
                    </a>
                    
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>
<!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar" style="color: white">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("roles.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection

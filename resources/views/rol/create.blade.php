@extends('layouts.template')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 col-md-offset-1" align="center">
      <div class="panel panel-default">
        <div class="panel-heading"><h2 style="padding-top: 2%">Crear Roles</h2></div>
          <div class="panel-body">

        	<form action="{{ route('roles.store') }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
              <div class="form-group">
                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control" name="nombre" required autofocus placeholder="Nombre" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input id="display_nombre" type="text" class="form-control" name="display_nombre" required autofocus placeholder="Slug">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                              <textarea rows="4" cols="10" name="descripcion" id="descripcion" class="form-control" required></textarea>
                             
                            </div>
                        </div>

                  <div align="center">
                    <h3>Permiso Especial</h3>
                  </div>


                  <div class="col-md-12 form-group">
                    
                    <select class="col-md-6 form-group" name="permiso_especial">
                      <option>Seleccionar Permiso Especial</option>
                    <option value="all-access">Acceso Total</option>
                    <option value="no-access">No acceso</option>
                    </select>
                  </div>


                <div class="form-group" align="center">
                <h3>Asignar Privilegios</h3>
                </div>
                <div style="overflow: scroll; height: 200px">
                  <div class="col-md-7 form-group" align="left">
                    <ul>
                      @foreach ($permisos as $permisos)
                    
                          <label>
                            <input  type="checkbox" value="{{$permisos->id }}" name="permisos[]">
                                         <strong>{{$permisos->name}}</strong><em>({{$permisos->description ?: 'Sin descripción'}})</em>
                            </label>
                          
                          @endforeach

                  </ul>
                  </div>
                </div>

                  <div class="text-center col-md-12" style="padding-top: 3%;padding-bottom: 2%">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('roles.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              </div>
            </form>
        </div>
              
@endsection
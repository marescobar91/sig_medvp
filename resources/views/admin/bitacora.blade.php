@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-12 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-md-12">

                        <h1 align="center">Bitacoras</h1>
                        
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                   
                    <th>Reporte o Consulta</th>
                    <th> Usuario</th>
                    <th>Fecha</th>
                    
                    <th>Hora</th>
                </tr>
                </thead>
                <tbody>
                   @foreach($bitacoras as $bitacora)
                <tr>
                    <td>{{ $bitacora->nombre_reporte }}</td>
                    <td>{{ $bitacora->user->nombre }} {{ $bitacora->user->apellido }}</td>
                    <td>{{ $bitacora->fecha_consulta }}</td>
                    <td>{{ $bitacora->hora_consulta }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
<!--Modal-->
        
        </div>
    </div>
@endsection

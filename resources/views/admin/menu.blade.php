@extends('layouts.template')
@section('content')
<div id="portfolio">
<header class="section-header">
          <h3 class="section-title">Menú Principal</h3>
        </header>

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li>Administrador</li>
            <!--  <li data-filter=".filter-web">Web</li>-->
            </ul>
          </div>
        </div>
        <div class="row portfolio-container col-lg-12" style="align-content: center">     
        <div class="col-lg-1 col-md-6 portfolio-item filter-card">
          </div>     
          <div class="col-lg-5 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web1.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-6  portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="img/portfolio/web2.png" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
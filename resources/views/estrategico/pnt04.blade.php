@extends('layouts.template')
@section('content')
<div id="contact">
  <div class="container-fluid">
        <div class="section-header">
          <h3>Reporte de Estadisticas de Visas Otorgadas</h3>
        </div>
          <div class="col-lg-12" align="center">
            <div class="row">
               @if($vacio ==null)
              <div class="col-lg-12">
                <h5>Seleccione la Fecha Inicio y Fecha Final</h5>
              </div>
              @endif
            </div>

            <div class="form" style="align-content: center;">
                           
              <form action="{{ route('visas.estadisticas') }}" method="post" role="form" class="contactForm">
                 {{ csrf_field() }}
                <div class="form-row">
                  <div class="col-lg-3">
                     @if($vacio ==null)
                    <label class="form-control" style="border:0px;"> Fecha Inicio: </label>
                    </div>
                     
                    <div class="form-group col-lg-3">
                    
                    <input type="date" name="fecha_inicio" class="form-control" id="name"/>
                    <div class="validation"></div>
                    </div>
                    <div class="col-lg-3">
                    <label class="form-control" style="border:0px;"> Fecha Final: </label>
                    </div>
                    <div class="form-group col-lg-3">
                    <input type="date" name="fecha_final" class="form-control" id="name"/>
                    <div class="validation"></div>
                    </div>
                 </div>                 
                <div class="text-center" style="padding-top: 3%;padding-bottom: 2%">
                    <button class="btn-primary">Consultar</button> 
                </div>
                  @endif
              </form>
</div>
                    <div class="col-md-12">
             <div>
                    @if($vacio!=null)
                    <h4 align="center" > <b>Total de Estadisticas de Visas Otorgadas</b>
                    <h5 class="col-md-6" style="float: left;">Desde : {{date('j', strtotime($finicio))}} de {{ $ife }} del {{date('Y', strtotime($finicio))}}</h4>
                    <h5 class="col-md-6" style="float: right;"> Hasta: {{date('j', strtotime($ffinal))}} de {{ $fe }} del {{date('Y', strtotime($ffinal))}}</b>
                        
                    </h4>
                    @endif
              </div>
              
            <table class="table table-striped table-hover col-md-5" id="reporte" style="background: #fff;width: 75%" >
                <thead>
                  @if($vacio !=null)
                  <tr>
                    <th width="60%">Region</th>
                    <th width="40%">Total</th>

                </tr>
                @endif
                </thead>
                <tbody>
                   @if($vacio != null)
                   <tr>
                     <td>Norte America</td>
                     <td>{{ $na }}</td>
                   </tr>
                <tr>
                    <td>Centro America</td>
                    <td>{{ $ca }}</td>
                </tr>
                <tr>
                  <td>Sur America</td>
                  <td>{{ $sa }}</td>
                </tr>
                <tr>
                    <td>Africa</td>
                    <td>{{$af }}</td>
                </tr>
                <tr>
                  <td>Asia</td>
                  <td>{{ $as }}</td>
                </tr>
                <tr>
                  <td>Europa</td>
                  <td>{{ $eu }}</td>
                </tr>
                <tr>
                  <td>Oceania</td>
                  <td>{{ $oc }}</td>
                </tr>
                
                 @else
                 <tr>
                   <td></td>
                   <td></td>
                 </tr>
                @endif
                </tbody>
                <tfoot>
                  @if($vacio != null)
                    <tr>
                        <td style="font-size: 1.5em"><b>Totales</b></td>
                        <td style="font-size: 1.5em"><b>{{ $total }}</b></td>
                    </tr>
                    @endif
                </tfoot>
               
            </table>

<div class="form" style="align-content: center;">
            <form action="{{ route('visas.pdfestadistica') }}" method="post" role="form" class="contactForm" target="_blank">
                 {{ csrf_field() }}
                <div class="">
                  @if($vacio != null)
                  <input id="fechaini" type="hidden" class="form-control" value="{{$finicio}}" name="fechaini">
                   <input id="fechafin" type="hidden" class="form-control" value="{{$ffinal}}" name="fechafin">
                    <button class="btn-primary">Generar PDF</button> 
                    <a class="btn btn-secondary" style="background: #dddddd; color: black" href="{{ route('visas.vervisa') }}">Resetear</a>
                </div>
                  @endif
              </form>
</div>      
        
          </div>

       </div>
             
            </div>
          </div>

        </div>

      </div>
@endsection
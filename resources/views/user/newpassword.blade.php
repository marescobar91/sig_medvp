@extends('layouts.template')
@section('content')
<div class="container">
       <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
  <div class="row">
    <div class="col-md-12 col-md-offset-1" align="center">
      <h1 style="padding-top: 2%">Cambiar Contraseña</h1>
      <form action="{{ route('user.updatepassword') }}" method="post" role="form" class="contactForm">
        {{csrf_field()}}
         <div class="form-group">
                            <div class="col-md-6 {{  errors->has('contraseña_actual') ? ' has-error' : '' }}">
                                <input id="contraseña_actual" type="password" class="form-control" name="contraseña_actual" required placeholder="Contraseña Actual">

                            </div>
                             <div class="validation">
                    @if ($errors->has('contraseña_actual'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contraseña_actual') }}</strong>
                                    </span>
                    @endif
                  </div>
                          </div>
                            <div class="form-group ">
                            <div class="col-md-6 {{  errors->has('contraseña_nueva') ? ' has-error' : '' }}">
                                <input id="contraseña_nueva" type="password" class="form-control" name="contraseña_nueva" required placeholder="Contraseña Nueva">

                            </div>
                             <div class="validation">
                    @if ($errors->has('contraseña_nueva'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contraseña_nueva') }}</strong>
                                    </span>
                    @endif
                  </div>
                          </div>
                            <div class="form-group">
                            <div class="col-md-6 {{  errors->has('confirmar_contraseña') ? ' has-error' : '' }}">
                                <input id="confirmar_contraseña" type="password" class="form-control" name="confirmar_contraseña" required placeholder="Confirmar Contraseña">

                            </div>
                            <div class="validation">
                    @if ($errors->has('confirmar_contraseña'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('confirmar_contraseña') }}</strong>
                                    </span>
                    @endif
                  </div>
                            </div>
                            <div class="col-md-12 form-group">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Cambiar Contraseña</button>
                    <a href="{{ route('home') }}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
      </form>
    </div>
  </div>
</div>
@endsection
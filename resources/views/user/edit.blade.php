@extends('layouts.template')
@section('content')

<div class="container">

  <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
  <div class="row">
    <div class="col-md-12 col-md-offset-1" align="center">
<h1 style="padding-top: 2%">Editar Usuario</h1>
  <form action="{{ route('user.update', $user->id) }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
    {{ method_field('PUT') }}
              
                <div class="form-group">
                             <label for="name" class="col-md-4 control-label">Nombre</label>
                            <div class="col-md-6 {{ $errors->has('nombre') ?  ' has-error' : '' }}">
                                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ $user->nombre}}" required autofocus placeholder="Nombre">

                            </div>
                            <div class="validation">
                    @if ($errors->has('nombre'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                    @endif
                  </div>
                  </div>
                  <div class="form-group">
                             <label for="name" class="col-md-4 control-label">Apellido</label>
                            <div class="col-md-6 {{  $errors->has('apellido') ? ' has-error' : '' }}">
                                <input id="apellido" type="text" class="form-control" name="apellido" value="{{ $user->apellido }}" required autofocus placeholder="Nombre">

                            </div>
                            <div class="validation">
                    @if ($errors->has('apellido'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('apellido') }}</strong>
                                    </span>
                    @endif
                  </div>
                  </div>
                  <div class="form-group">
                             <label for="usuario" class="col-md-4 control-label">Nombre de Usuario</label>
                            <div class="col-md-6 {{  $errors->has('usuario') ? ' has-error' : '' }}">
                                <input id="usuario" type="text" class="form-control" name="usuario" value="{{ $user->username }}" required autofocus placeholder="Nombre">

                            </div>

                            <div class="validation">
                    @if ($errors->has('usuario'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('usuario') }}</strong>
                                    </span>
                    @endif
                  </div>
                  </div>
            <div class="col-md-8 form-group">
              <hr>
              <h3>Lista de Roles</h3>
                </div>
          <div class="col-md-7 form-group {  $errors->has('roles') ? ' has-error' : '' }}" align="left">
            <ul>
              @foreach ($roles as $role)
            
                  <label>
                              <input type="checkbox" value="{{$role->id}}"
                                               {{in_array($role->id, $rol) ? "checked" : null}} name="roles[]">
                                          {{$role->name}}</strong><em>({{$role->description ?: 'Sin descripción'}})</em>
                            </label>    
                  
                  @endforeach

          </ul>
          <div class="validation">
                    @if ($errors->has('roles'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                    @endif
                  </div>
          </div>
                  <div class="span8 form-group">
                    <button class="btn btn-primary" type="submit">Guardar</button>
                     <a href="{{route('user.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
              
              
            </form>
        </div>
       </div>
       </div>       
@endsection

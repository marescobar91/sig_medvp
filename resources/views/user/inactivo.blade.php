@extends('layouts.template')
@section('content')

<div class="container" style="margin-top: 3%">
        <div class="col-md-12 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-md-12">
                        <h1 align="center">Gestion de Usuarios Inactivos</h1>
                    </div>
                      
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>N°</td>
                    <th>Nombre Completo</th>
                    <th>Nombre de Usuario</th>
                    
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->nombre}} {{ $user->apellido }}</td>
                    <td>{{ $user->username }}</td>
                    <td>
                    @can('user-habilitar')
                <a style="margin-right: 3%" class="btn btn-primary" onclick="eliminar('{{ $user->id }}')">
                    <span class="ion-android-unlock" style="color: white"></span></a>
                    @endcan   
                    </td> 
                </tr>
                @endforeach
                </tbody>
            </table>
         <!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Habilitar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-primary" id="aceptar" style="color: white">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("user.habilitar", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection

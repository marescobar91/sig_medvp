@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-12 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-md-12">

                        <h1 align="center">Seguridad y Roles</h1>
                        <div style="padding-left: 50%">
                        <a href="{{ route('roles.index') }}" class="btn btn-primary"> <span>
                            <b>Gestion de Roles</b></span></a>
                             <a href="{{ route('bitacoras.index') }}" class="btn btn-primary"> <span>
                            <b>Bitacoras</b></span></a></div>
                     </div>

                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>N°</td>
                    <th>Nombre Completo</th>
                    <th> Nombre de Usuario</th>
                    <th> Roles</th>
                    
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->nombre}} {{ $user->apellido }}</td>
                    <td>{{ $user->username }}</td>
                   <td> <ul>
                    @foreach($user->roles as $rol)
                        <li>
                            {{$rol->name}}
                        </li>
                      @endforeach
                    </ul>
                </td>
                  <td>
                    @if($user->id != $id)
               
                <a href="{{ route('admin.password', $user->id) }}"  class="btn btn-info"><span class="ion-key"></span></a>
                
                    @endif
                    
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>

            
        </div>
    </div>
@endsection

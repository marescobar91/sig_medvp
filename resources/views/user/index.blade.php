@extends('layouts.template')
@section('content')
<div class="container" style="margin-top: 3%">
        <div class="col-md-12 col-md-offset-1">
            <div class="table-title" style=" margin-bottom: 1%">
                <div class="row">
                    <div class="col-md-12">

                        <h1 align="center">Gestion de Usuarios</h1>
                        <div style="padding-left: 70%">
                            @can('user-inactivo')
                        <a href="{{ route('user.inactivo') }}" class="btn btn-primary"> <span>
                            @endcan
                            <b>Usuarios Inactivos</b></span></a></div>
                    </div>
                </div>
            </div>
            <table class="table table-striped table-hover" id="myTable" style="background: #fff;width: 100%">
                <thead>
                <tr>
                    <th>N°</td>
                    <th>Nombre Completo</th>
                    <th> Nombre de Usuario</th>
                    
                    <th width="23%">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($user as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->nombre}} {{ $user->apellido }}</td>
                    <td>{{ $user->username }}</td>
                   
                        <td>
                    @can('user-edit')
                        <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning"><span class="ion-edit" style="color: white"></span> </a>
                    @endcan
                   
                        @if($user->id != $id)
                     @can('user-delete')
                        <a class="btn btn-danger" onclick="eliminar('{{ $user->id }}')"><span class="ion-trash-a" style="color: white"></span>
                    </a>
                    @endcan
                    @endif
                   
                      </td>
                </tr>
                @endforeach
                </tbody>
            </table>
<!--Modal-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <p> ¿Desea Eliminar? </p>
                    <input type="hidden">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" id="aceptar" style="color: white">Sí</a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    
    function eliminar(id){
        var iddentified=id;
        
        $('#exampleModal').modal();
        document.getElementById("aceptar").addEventListener("click", function() {
        var url ='{{ route("user.destroy", ":id") }}';
        url=url.replace(":id",iddentified);
        window.location.href=url;
        }, false);

    }
    </script>
@endsection

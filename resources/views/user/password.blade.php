@extends('layouts.template')
@section('content')
<div class="container">
         <!-- Despliega mensajes de error -->
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <button type="button" class="close" data-dismiss="alert">
                                &times;</button>
                                <strong>¡ERROR!</strong> Hay errores en la información ingresada.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
@endif
    <div class="row">
        <div class="col-md-12 col-md-offset-1" align="center">
            <div class="panel panel-default">
                <div class="panel-heading"><h2 style="padding-top: 2%">Cambiar Contraseña</h2></div>
                <div class="panel-body">
                	<form action="{{ route('admin.changepassword', $user->id) }}" method="post" role="form" class="contactForm">
    {{csrf_field()}}
                        <div class="form-group">
                            <div class="col-md-6 {{  errors->has('contraseña') ? ' has-error' : '' }}">
                                <input id="contraseña" type="password" class="form-control" name="contraseña" required autofocus placeholder="Contraseña">
                            </div>
                            <div class="validation">
                    @if ($errors->has('contraseña'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('contraseña') }}</strong>
                                    </span>
                    @endif
                  </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 {{  errors->has('confirmar_contraseña') ? ' has-error' : '' }}">
                                <input id="confirmar_contraseña" type="password" class="form-control" name="confirmar_contraseña" required autofocus placeholder="Confirmar Contraseña">
                                @if ($errors->has('confirmar_contraseña'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('confirmar_contraseña') }}</strong>
                                    </span>
                    @endif
                            </div>
                     
                <div class="col-md-8 form-group" style="padding-top: 3%">
                  <div class="text-center">
                    <button class="btn btn-primary" type="submit">Cambiar Contraseña</button>
                    <a href="{{route('user.index')}}" class="btn btn-secondary" style="float:right 5%;"> <span>
                            <b>Cancelar</b></span></a>
                  </div>
                </div>
              
            </form>
        </div>
      </div>
    </div>
  </div>
</div>              
@endsection
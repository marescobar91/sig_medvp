<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;

class VisaMultiple extends Model
{
    //
    protected $table="visa_multiple"; 

    protected $fillable =['solicitante_id', 'estado_id', 'fecha_ingreso', 'fecha_residencia', 'cal_migratoria', 'actividad_realizada', 'autorizado', 'fecha_desde', 'fecha_hasta', 'dictamen', 'fecha_servidor'];


    public function solicitante(){

    	return $this->belongsTo('App\Solicitante', 'solicitante_id');
    }

    public function estado(){

    	return $this->belongsTo('App\Estado', 'estado_id');
    }

    public function pariente(){

    	return $this->hasMany('App\Pariente');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UbicacionGeografica extends Model
{
    
     protected $table="ubicacion_geografica"; 

    protected $fillable =['nombre', 'abreviatura', 'abreviatura_lata', 'nivel', 'ubicacion_superior'];


    public function solicitante (){

    	return $this->hasMany('App\Solicitante');
    }

    public function ordensalida(){

    	return $this->hasMany('App\OrdenSalida');
    }

    public function visasalida(){
    	return $this->hasMany('App\VisaSalida');
    }

     public function pariente(){

        return $this->hasMany('App\Pariente');
    }


 }

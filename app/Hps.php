<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hps extends Model
{
    //
    protected $table="hps"; 

    protected $fillable =['solicitante_id', 'estado_id', 'fecha_entrada', 'fecha_salida', 'motivo', 'autorizado', 'fecha_desde', 'fecha_hasta', 'observacion', 'fecha_servidor'];
    

    public function solicitante(){

    	return $this->belongsTo('App\Solicitante', 'solicitante_id');
    }

    public function estado(){

    	return $this->belongsTo('App\Estado', 'estado_id');
    }

    public function pariente(){

    	return $this->hasMany('App\Pariente');
    }
}

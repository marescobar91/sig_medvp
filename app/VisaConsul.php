<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisaConsul extends Model
{
    protected $table="visa_consul"; 

    protected $fillable =['solicitante_id', 'estado_id', 'f_entradaca4', 'fecha_salida', 'proposito', 'autorizado', 'fecha_desde', 'fecha_hasta', 'fecha_servidor'];


    public function solicitante(){

    	return $this->belongsTo('App\Solicitante', 'solicitante_id');
    }

    public function estado(){

    	return $this->belongsTo('App\Estado', 'estado_id');
    }
}

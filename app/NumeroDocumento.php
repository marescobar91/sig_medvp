<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NumeroDocumento extends Model
{
    protected $table="numero_documento"; 
    protected $fillable =['documento_id', 'solicitante_id', 'num_documento'];


    public function documento(){

    	return $this->belongsTo('App\TipoDocumento', 'documento_id');
    }

    public function solicitante(){

    	return $this->belongsTo('App\Solicitante', 'solicitante_id');
    }


}

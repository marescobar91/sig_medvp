<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NumeroPasaporte extends Model
{
    protected $table="numeropasaporte"; 

   
    protected $fillable =['pasaporte_id', 'solicitante_id', 'numeropasaporte'];

    public function pasaporte(){

    	return $this->belongsTo('App\TipoPasaporte');
    }

    public function solicitante(){

    	return $this->hasMany('App\Solicitante', 'solicitante_id');
    }
    
}

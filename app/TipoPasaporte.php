<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPasaporte extends Model
{
     protected $table="tipopasaporte"; 

    protected $fillable =['tipopasaporte'];

    public function numeropasaporte(){

    	return $this->hasMany('App\NumeroPasaporte');
    }
}

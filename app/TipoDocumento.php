<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoDocumento extends Model
{
    
     protected $table="tipo_documento"; 

    protected $fillable =['tipo_documento'];


    public function numerodocumento(){

    	return $this->hasMany('App\NumeroDocumento');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitante extends Model
{
    protected $table="solicitante"; 
    protected $fillable =['ubicacion_geografica_id', 'profesion_id', 'genero', 'fecha_nacimiento', 'edad', 'tramite', 'fecha_movimiento', 'fecha_servidor'];


    public function hps(){

    	return $this->hasMany('App\Hps');
    }
  
    public function numerodocumento(){

    	return $this->hasMany('App\NumeroDocumento');
    }

    public function numeropasaporte(){

    	return $this->hasMany('App\NumeroPasaporte');
    }

    public function ordensalida(){

    	return $this->hasMany('App\OrdenSalida');
    }

    public function ubicacion(){

    	return $this->belongsTo('App\UbicacionGeografica', 'ubicacion_geografica_id');
    }

    public function profesion(){

    	return $this->belongsTo('App\Profesion', 'profesion_id');
    }

    public function prorroga(){

    	return $this->hasMany('App\Prorroga');
    }

    public function visaconsul(){

        return $this->hasMany('App\VisaConsul');
    }

    public function visamultiple(){

        return $this->hasMany('App\VisaMultiple');
    }

    public function visasalida(){

        return $this->hasMany('App\VisaSalida');
    }



}

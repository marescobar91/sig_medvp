<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    
    protected $table="profesion"; 

    protected $fillable =['nombre_profesion'];


    public function solicitante(){

    	return $this->hasMany('App\Solicitante');
    } 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    
    protected $table="bitacoras"; 

    protected $fillable =['user_id', 'nombre_reporte', 'fecha_consulta', 'hora_consulta'];

    public function user(){

    	return $this->belongsTo('App\User');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prorroga extends Model
{
     protected $table="prorroga"; 

    protected $fillable =['solicitante_id', 'estado_id', 'fecha_ingreso', 'fecha_residencia', 'trabajo', 'actividad_realizar', 'oferta_trabajo', 'prov_capital', 'autorizado', 'fecha_desde', 'fecha_hasta', 'fecha_servidor'];



    public function solicitante(){

    	return $this->belongsTo('App\Solicitante', 'solicitante_id');
    }

    public function estado(){

    	return $this->belongsTo('App\Estado', 'estado_id');
    }

     public function pariente(){

    	return $this->hasMany('App\Pariente');
    }
}

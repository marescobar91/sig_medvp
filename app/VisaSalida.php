<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisaSalida extends Model
{
     protected $table="visa_salida"; 

    protected $fillable =['solicitante_id', 'ubicacion_geografica_id', 'estado_id', 'fecha_entrada', 'fecha_salida', 'via_salida', 'proposito', 'motivo', 'cde_respaldo', 'autorizado', 'fecha_desde', 'fecha_hasta', 'observacion', 'fecha_servidor'];


    public function solicitante(){
    	return $this->belongsTo('App\Solicitante', 'solicitante_id');
    }


    public function ubicacion(){

    	return $this->belongsTo('App\UbicacionGeografica', 'ubicacion_geografica_id');
    }


    public function estado(){

    	return $this->belongsTo('App\Estado', 'estado_id');
    }
}

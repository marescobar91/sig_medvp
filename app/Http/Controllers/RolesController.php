<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;
use Laracasts\Flash\Flash;

class RolesController extends Controller
{


    public function index(){

        $roles = Role::orderBy('id', 'DESC')->get();
        return view('rol.index')->with('rol', $roles);

    }

    
    public function create(){

    	$permisos = Permission::get();

    	return view('rol.create', compact('permisos'));
    }


    public function store(Request $request){


    	$role = new Role();
    	$role->name = $request->nombre;
    	$role->slug = $request->display_nombre;
    	$role->description = $request->descripcion;
        
        $role->save();


        //actualizar role
        $role->permissions()->sync($request->get('permisos'));
        Flash("¡Se ha registrado el rol " . $role->name . " de forma exitosa!")->success()->important();

        return redirect()->action('RolesController@index');

    }

    public function edit($id){

        $roles = Role::find($id);
        $permisos = Permission::all();
        $permission= $roles->permissions->pluck('id')->ToArray();

       //dd($permission);

        return view('rol.edit')->with('roles', $roles)->with('permisos', $permisos)->with('permission', $permission);
    }

    public function update(Request $request, $id){

        //actualiza primero al usuario
        //segundo se actualiza al rol

        //actualiza el usuario
        $role = Role::find($id);
        $role->name = $request->nombre;
        $role->slug = $request->display_nombre;
        $role->description = $request->descripcion;
        $role->update();

        //actualizar role
        $role->permissions()->sync($request->get('permissions'));
         Flash("¡Se ha editado el rol " . $role->name . " de forma exitosa!")->warning()->important();
        return redirect()->route('roles.index');
    }

   

    public function destroy($id){

        $role = Role::find($id);
        $role->delete();
         Flash("¡Se ha eliminado el rol " . $role->name . " de forma exitosa!")->error()->important();
        return redirect()->route('roles.index');

    }
}

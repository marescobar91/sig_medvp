<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hps;
use App\VisaConsul;
use App\VisaMultiple;
use App\Solicitante;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use \PDF;
use App\Bitacora;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class TramitesController extends Controller
{
    public function estadisticas(){

        $vacio="";

        return view('estrategico.pnt05')->with('vacio');
    }


    public function estadisticaTotal(Request $request){

        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){
        if($fecha_final < $fecha_inicio){ //validacion de fechas de filtros

      flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('VisasController@estadisticas');
        }
        if($fecha_inicio > $date){//fecha de filtro inicio versus fecha actual 

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('VisasController@estadisticas');
        }
        if($fecha_final > $date){//fecha final del filtro versus fecha actual

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('VisasController@estadisticas');
        }


        $vacio="Existe";

        $visas_consul = DB::table('visa_consul')->leftJoin('solicitante', 'visa_consul.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_consul.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_consul.solicitante_id) as total_visas'))->whereBetween('visa_consul.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_multiple = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visas'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $hps = DB::table('hps')->leftJoin('solicitante', 'hps.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'hps.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(hps.solicitante_id) as total_visas'))->whereBetween('hps.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $orden_salida = DB::table('orden_salida')->leftJoin('solicitante', 'orden_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(orden_salida.solicitante_id) as total_visas'))->whereBetween('orden_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('orden_salida.autorizado', '=', 'Si')->groupBy('ubicacion_geografica.abreviatura_lata');

        $visa_salida = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_salida.solicitante_id) as total_visas'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $prorroga = DB::table('prorroga')->leftJoin('solicitante', 'prorroga.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'prorroga.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(prorroga.solicitante_id) as total_visas'))->whereBetween('prorroga.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_consultadas = $hps->union($visas_multiple)->union($visas_consul)->union($orden_salida)->union($visa_salida)->union($prorroga)->get();

      
         $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte total de tramites por nacionalidad.";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();      


        //dd($visas_consultadas);
       }

        $ca = 0;
        foreach($visas_consultadas as $visas){
            if($visas->abreviatura_lata == 'CA')
                $ca = $visas->total_visas + $ca;
        }

        $sa=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='SA')
                $sa = $visas->total_visas + $sa;
        }

        $na=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='NA')
                $na = $visas->total_visas + $na;
        }

        $eu=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='EU')
                $eu = $visas->total_visas + $eu;
        }


        $af=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AF')
                $af = $visas->total_visas + $af;
        }


        $as=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AS')
                $as = $visas->total_visas + $as;
        }


        $oc=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='OC')
                $oc = $visas->total_visas + $oc;
        }

        $total = 0;
        foreach($visas_consultadas as $visas){

        	$total = $visas->total_visas + $total;
        }

return view('estrategico.pnt05')->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('ca', $ca)->with('sa', $sa)->with('na', $na)->with('eu', $eu)->with('af', $af)->with('as', $as)->with('oc', $oc)->with('total', $total);
    }


    public function tramitesPdf(Request $request){

    	$fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}


        if($fecha_inicio!=null && $fecha_final!=null){

        $visas_consul = DB::table('visa_consul')->leftJoin('solicitante', 'visa_consul.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_consul.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_consul.solicitante_id) as total_visas'))->whereBetween('visa_consul.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_multiple = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visas'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $hps = DB::table('hps')->leftJoin('solicitante', 'hps.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'hps.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(hps.solicitante_id) as total_visas'))->whereBetween('hps.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $orden_salida = DB::table('orden_salida')->leftJoin('solicitante', 'orden_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(orden_salida.solicitante_id) as total_visas'))->whereBetween('orden_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('orden_salida.autorizado', '=', 'Si')->groupBy('ubicacion_geografica.abreviatura_lata');

        $visa_salida = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_salida.solicitante_id) as total_visas'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $prorroga = DB::table('prorroga')->leftJoin('solicitante', 'prorroga.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'prorroga.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(prorroga.solicitante_id) as total_visas'))->whereBetween('prorroga.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_consultadas = $hps->union($visas_multiple)->union($visas_consul)->union($orden_salida)->union($visa_salida)->union($prorroga)->get();

         $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte total de tramites por nacionalidad, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();  

        //dd($visas_consultadas);
       }

        $ca = 0;
        foreach($visas_consultadas as $visas){
            if($visas->abreviatura_lata == 'CA')
                $ca = $visas->total_visas + $ca;
        }

        $sa=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='SA')
                $sa = $visas->total_visas + $sa;
        }

        $na=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='NA')
                $na = $visas->total_visas + $na;
        }

        $eu=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='EU')
                $eu = $visas->total_visas + $eu;
        }


        $af=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AF')
                $af = $visas->total_visas + $af;
        }


        $as=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AS')
                $as = $visas->total_visas + $as;
        }


        $oc=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='OC')
                $oc = $visas->total_visas + $oc;
        }

        $total = 0;
        foreach($visas_consultadas as $visas){

        	$total = $visas->total_visas + $total;
        }

  $pdf = \PDF::loadView('estrategico.rpt05', compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'ca', 'sa', 'na', 'eu', 'af', 'as', 'oc', 'total'))->setWarnings(false);
        return $pdf->stream('reporteRPT05.pdf');

    }
    
}

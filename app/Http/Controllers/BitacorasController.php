<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Bitacora;
use Caffeinated\Shinobi\Models\Role;
use Validator;
use Auth;
use Hash;
use Laracasts\Flash\Flash;

class BitacorasController extends Controller
{
    //

    public function index(){

    	$bitacoras = Bitacora::all();

    	return view('admin.bitacora')->with('bitacoras', $bitacoras);

    }
}

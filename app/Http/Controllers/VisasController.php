<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hps;
use App\VisaConsul;
use App\VisaMultiple;
use App\Solicitante;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use \PDF;
use App\Bitacora;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class VisasController extends Controller
{
    //

    public function ver(){

    	$vacio="";
    	return view('estrategico.pnt02', compact('vacio'));
    }

    public function visas(Request $request){

    	$fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){
       	if($fecha_final < $fecha_inicio){

      flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('VisasController@ver');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('VisasController@ver');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('VisasController@ver');
        }
       	$vacio="Existe";

       	$visas_consul = DB::table('visa_consul')->leftJoin('solicitante', 'visa_consul.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'visa_consul.estado_id')->select('solicitante.genero', DB::raw('COUNT(visa_consul.solicitante_id) as total_visas'))->whereBetween('visa_consul.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('solicitante.genero');

       	$visas_multiple = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'visa_multiple.estado_id')->select('solicitante.genero', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visas'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado', '=', 1)->groupBy('solicitante.genero');

       	$visas_salida = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'visa_salida.estado_id')->select('solicitante.genero', DB::raw('COUNT(visa_salida.solicitante_id) as total_visas'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado', '=', 1)->groupBy('solicitante.genero');

       	$visas_consultadas = $visas_salida->union($visas_multiple)->union($visas_consul)->get();


         $noVisa= $visas_consultadas->isEmpty();
                
        if($noVisa == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('VisasController@ver');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Visas Otorgadas";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

       }

        $masculino = 0;
        foreach($visas_consultadas as $visas){
        	if($visas->genero == 'M')
        		$masculino = $visas->total_visas + $masculino;
        }

        $femenino=0;
        foreach($visas_consultadas as $visas){

        	if($visas->genero=='F')
        		$femenino = $visas->total_visas + $femenino;
        }
        $total = 0;
        foreach($visas_consultadas as $visas){

        	$total = $visas->total_visas + $total;
        }   
return view('estrategico.pnt02')->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('femenino', $femenino)->with('masculino', $masculino)->with('total', $total);
    }


    public function pdfVisas(Request $request){

    	$fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}


       if($fecha_inicio!=null && $fecha_final!=null){
       	$visas_consul = DB::table('visa_consul')->leftJoin('solicitante', 'visa_consul.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'visa_consul.estado_id')->select('solicitante.genero', DB::raw('COUNT(visa_consul.solicitante_id) as total_visas'))->whereBetween('visa_consul.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('solicitante.genero');

        $visas_multiple = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'visa_multiple.estado_id')->select('solicitante.genero', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visas'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado', '=', 1)->groupBy('solicitante.genero');
        
        $visas_salida = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'visa_salida.estado_id')->select('solicitante.genero', DB::raw('COUNT(visa_salida.solicitante_id) as total_visas'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado', '=', 1)->groupBy('solicitante.genero');

        $visas_consultadas = $visas_salida->union($visas_multiple)->union($visas_consul)->get();

         $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Visas Otorgadas, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

       	}

       	$masculino = 0;
        foreach($visas_consultadas as $visas){
        	if($visas->genero == 'M')
        		$masculino = $visas->total_visas + $masculino;
        }

        $femenino=0;
        foreach($visas_consultadas as $visas){

        	if($visas->genero=='F')
        		$femenino = $visas->total_visas + $femenino;
        }
        $total = 0;
        foreach($visas_consultadas as $visas){

        	$total = $visas->total_visas + $total;
        }

      
       	$pdf = \PDF::loadView('estrategico.rpt03', compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'femenino', 'masculino', 'total'))->setWarnings(false);
        return $pdf->stream('reporteRPT03.pdf');
    	
    }


    public function verEstadisticas(){

        $vacio="";

        return view('estrategico.pnt04')->with('vacio', $vacio);
    }

    public function estadisticasVisas(Request $request){

        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){
        if($fecha_final < $fecha_inicio){

      flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('VisasController@verEstadisticas');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('VisasController@verEstadisticas');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('VisasController@verEstadisticas');
        }
        $vacio="Existe";

        $visas_consul = DB::table('visa_consul')->leftJoin('solicitante', 'visa_consul.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_consul.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_consul.solicitante_id) as total_visas'))->whereBetween('visa_consul.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_multiple = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visas'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visa_salida = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_salida.solicitante_id) as total_visas'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_consultadas = $visa_salida->union($visas_multiple)->union($visas_consul)->get();


         $noVisa= $visas_consultadas->isEmpty();
                
        if($noVisa == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('VisasController@verEstadisticas');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Estadisticas de Visas";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

       }
    $ca = 0;
        foreach($visas_consultadas as $visas){
            if($visas->abreviatura_lata == 'CA')
                $ca = $visas->total_visas + $ca;
        }

        $sa=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='SA')
                $sa = $visas->total_visas + $sa;
        }

        $na=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='NA')
                $na = $visas->total_visas + $na;
        }

        $eu=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='EU')
                $eu = $visas->total_visas + $eu;
        }


        $af=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AF')
                $af = $visas->total_visas + $af;
        }


        $as=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AS')
                $as = $visas->total_visas + $as;
        }


        $oc=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='OC')
                $oc = $visas->total_visas + $oc;
        }

        $total = 0;
        foreach($visas_consultadas as $visas){

            $total = $visas->total_visas + $total;
        }
 
return view('estrategico.pnt04')->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('ca', $ca)->with('sa', $sa)->with('na', $na)->with('eu', $eu)->with('af', $af)->with('as', $as)->with('oc', $oc)->with('total', $total);

    }

    public function estadisticaPdf(Request $request){

        $fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
         Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

        if($fecha_inicio!=null && $fecha_final!=null){
            $visas_consul = DB::table('visa_consul')->leftJoin('solicitante', 'visa_consul.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_consul.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_consul.solicitante_id) as total_visas'))->whereBetween('visa_consul.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_multiple = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visas'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visa_salida = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(visa_salida.solicitante_id) as total_visas'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $visas_consultadas = $visa_salida->union($visas_multiple)->union($visas_consul)->get();

         $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Estadisticas de Visas, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
        }

        $ca = 0;
        foreach($visas_consultadas as $visas){
            if($visas->abreviatura_lata == 'CA')
                $ca = $visas->total_visas + $ca;
        }

        $sa=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='SA')
                $sa = $visas->total_visas + $sa;
        }

        $na=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='NA')
                $na = $visas->total_visas + $na;
        }

        $eu=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='EU')
                $eu = $visas->total_visas + $eu;
        }


        $af=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AF')
                $af = $visas->total_visas + $af;
        }


        $as=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='AS')
                $as = $visas->total_visas + $as;
        }


        $oc=0;
        foreach($visas_consultadas as $visas){

            if($visas->abreviatura_lata=='OC')
                $oc = $visas->total_visas + $oc;
        }

        $total = 0;
        foreach($visas_consultadas as $visas){

            $total = $visas->total_visas + $total;
        }
    $pdf = \PDF::loadView('estrategico.rpt04', compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total', 'ca', 'sa', 'na', 'eu', 'af', 'as', 'oc'))->setWarnings(false);
        return $pdf->stream('reporteRPT04.pdf');

    }
    
}

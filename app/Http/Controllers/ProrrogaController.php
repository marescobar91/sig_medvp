<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Prorroga;
use App\Solicitante;
use App\Pariente;
use App\Hps;
use \PDF;
use App\Bitacora;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;



class ProrrogaController extends Controller
{
    //
    public function __construct(){

        $this->middleware('permission:menu-estrategico')->only('ver');
        $this->middleware('permission:reporte-estrategico')->only('prorroga');
        
    }

    public function ver(){

    	$vacio = "";

    	return view('estrategico.pnt01')->with('vacio', $vacio);
    }


    public function prorroga(Request $request){
    	$fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('ProrrogaController@ver');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('ProrrogaController@ver');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('ProrrogaController@ver');
        }

        $vacio="Existe";

        $prorroga = DB::table('prorroga')->leftJoin('solicitante', 'prorroga.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'prorroga.estado_id')->select('solicitante.genero', DB::raw('COUNT(prorroga.solicitante_id) as total_prorroga'))->whereBetween('prorroga.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('solicitante.genero');

        $hps = DB::table('hps')->leftJoin('solicitante', 'hps.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'hps.estado_id')->select('solicitante.genero', DB::raw('COUNT(hps.solicitante_id) as total_prorroga'))->whereBetween('hps.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('solicitante.genero');

        $prorrogas = $prorroga->union($hps)->get();

        //dd($prorrogas);
        $noProrroga= $prorrogas->isEmpty();        
        if($noProrroga == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('ProrrogaController@ver');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Prorrogas Otorgadas";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();        
            }
        $masculino = 0;
        foreach($prorrogas as $prorroga){

            if($prorroga->genero == 'M')
            $masculino = $prorroga->total_prorroga + $masculino;
        }

        $femenino=0;
        foreach($prorrogas as $prorroga){
            if($prorroga->genero=='F')
                $femenino = $prorroga->total_prorroga + $femenino;
        }
        $total = 0;
        foreach($prorrogas as $prorroga){
            $total = $prorroga->total_prorroga + $total;
        }   
       		//dd($prorrogas);
return view('estrategico.pnt01')->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('femenino', $femenino)->with('masculino', $masculino)->with('total', $total);
            
    }

    public function pdfProrroga(Request $request){


    	$fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
         Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

      $prorroga = DB::table('prorroga')->leftJoin('solicitante', 'prorroga.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'prorroga.estado_id')->select('solicitante.genero', DB::raw('COUNT(prorroga.solicitante_id) as total_prorroga'))->whereBetween('prorroga.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('solicitante.genero');

        $hps = DB::table('hps')->leftJoin('solicitante', 'hps.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'hps.estado_id')->select('solicitante.genero', DB::raw('COUNT(hps.solicitante_id) as total_prorroga'))->whereBetween('hps.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('solicitante.genero');

         $prorrogas = $prorroga->union($hps)->get();

          $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Prorrogas Otorgadas, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save(); 

       	}

        $masculino = 0;
        foreach($prorrogas as $prorroga){

            if($prorroga->genero == 'M')
            $masculino = $prorroga->total_prorroga + $masculino;
        }

        $femenino=0;
        foreach($prorrogas as $prorroga){
            if($prorroga->genero=='F')
                $femenino = $prorroga->total_prorroga + $femenino;
        }
        $total = 0;
        foreach($prorrogas as $prorroga){
            $total = $prorroga->total_prorroga + $total;
        } 

       	//dd($prorrogas);
       	$pdf = \PDF::loadView('estrategico.rpt01', compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'femenino', 'masculino', 'total'))->setWarnings(false);
        return $pdf->stream('reportePT01.pdf');
    }

    public function verProrroga(){

        $vacio="";

        return view('estrategico.pnt03')->with('vacio', $vacio);
    }

 
    public function estadisticaProrrogas(Request $request){

        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('ProrrogaController@verProrrogas');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('ProrrogaController@verProrrogas');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('ProrrogaController@verProrrogas');
        }
        $prorroga = DB::table('prorroga')->leftJoin('solicitante', 'prorroga.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'prorroga.estado_id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(prorroga.solicitante_id) as total_prorroga'))->whereBetween('prorroga.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $hps = DB::table('hps')->leftJoin('solicitante', 'hps.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'hps.estado_id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(hps.solicitante_id) as total_prorroga'))->whereBetween('hps.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $prorrogas = $prorroga->union($hps)->get();
        //dd($prorrogas);
        $vacio="Existe";

        $noProrroga= $prorrogas->isEmpty();
                
        if($noProrroga == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('ProrrogaController@verProrrogas');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Estadisticas Prorrogas por Nacionalidad";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

            }
        $ca = 0;
        foreach($prorrogas as $prorroga){
            if($prorroga->abreviatura_lata == 'CA')
                $ca = $prorroga->total_prorroga + $ca;
        }

        $sa=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='SA')
                $sa = $prorroga->total_prorroga + $sa;
        }

        $na=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='NA')
                $na = $prorroga->total_prorroga + $na;
        }

        $eu=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='EU')
                $eu = $prorroga->total_prorroga + $eu;
        }


        $af=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='AF')
                $af = $prorroga->total_prorroga + $af;
        }


        $as=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='AS')
                $as = $prorroga->total_prorroga + $as;
        }


        $oc=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='OC')
                $oc = $prorroga->total_prorroga + $oc;
        }

        $total = 0;
        foreach($prorrogas as $prorroga){

            $total = $prorroga->total_prorroga + $total;
        }
return view('estrategico.pnt03')->with('total', $total)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('vacio', $vacio)->with('ife', $ife)->with('fe', $fe)->with('ca', $ca)->with('sa', $sa)->with('na', $na)->with('eu', $eu)->with('af', $af)->with('as', $as)->with('oc', $oc);
     
    }

    public function pdfEstadistica(Request $request){

        $fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){
        $prorroga = DB::table('prorroga')->leftJoin('solicitante', 'prorroga.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'prorroga.estado_id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(prorroga.solicitante_id) as total_prorroga'))->whereBetween('prorroga.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $hps = DB::table('hps')->leftJoin('solicitante', 'hps.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'estado.id', '=', 'hps.estado_id')->leftJoin('ubicacion_geografica', 'ubicacion_geografica.id', '=', 'solicitante.ubicacion_geografica_id')->select('ubicacion_geografica.abreviatura_lata', DB::raw('COUNT(hps.solicitante_id) as total_prorroga'))->whereBetween('hps.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.abreviatura_lata');

        $prorrogas = $prorroga->union($hps)->get();

        $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Estadisticas Prorrogas por Nacionalidad, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
        }

        $ca = 0;
        foreach($prorrogas as $prorroga){
            if($prorroga->abreviatura_lata == 'CA')
                $ca = $prorroga->total_prorroga + $ca;
        }

        $sa=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='SA')
                $sa = $prorroga->total_prorroga + $sa;
        }

        $na=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='NA')
                $na = $prorroga->total_prorroga + $na;
        }

        $eu=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='EU')
                $eu = $prorroga->total_prorroga + $eu;
        }


        $af=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='AF')
                $af = $prorroga->total_prorroga + $af;
        }


        $as=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='AS')
                $as = $prorroga->total_prorroga + $as;
        }


        $oc=0;
        foreach($prorrogas as $prorroga){

            if($prorroga->abreviatura_lata=='OC')
                $oc = $prorroga->total_prorroga + $oc;
        }

        $total = 0;
        foreach($prorrogas as $prorroga){

            $total = $prorroga->total_prorroga + $total;
        }

 $pdf = \PDF::loadView('estrategico.rpt02', compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total', 'ca', 'sa', 'na', 'eu', 'af', 'as', 'oc'))->setWarnings(false);
        return $pdf->stream('reportePT02.pdf');
    }

    public function verProrrogaPTNAHPSyPTNAHPE(){

        $vacio="";

        return view('tactico.pnt09')->with('vacio', $vacio);
    }

    public function prorrogaPTNAHPSyPTNAHPE(Request $request){
        //metodo del hijos salvadoreños y edades 
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

        if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('ProrrogaController@verProrrogaPTNAHPSyPTNAHPE');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('ProrrogaController@verProrrogaPTNAHPSyPTNAHPE');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('ProrrogaController@verProrrogaPTNAHPSyPTNAHPE');
        }

        $vacio ="Existe";

        $hijosSalvador = DB::table('hps')->leftJoin('solicitante','hps.solicitante_id','=','solicitante.id')->select('solicitante.edad', DB::raw('COUNT(hps.solicitante_id) as hijo'))->whereBetween('hps.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->groupBy('solicitante.edad')->get();

        $totalH = DB::table('hps')->leftJoin('solicitante','hps.solicitante_id','=','solicitante.id')->whereBetween('hps.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->count('hps.solicitante_id');


        $extranjero = DB::table('prorroga')->leftJoin('solicitante','prorroga.solicitante_id','=','solicitante.id')->select('solicitante.edad', DB::raw('COUNT(prorroga.solicitante_id) as extranjero'))->whereBetween('prorroga.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->groupBy('solicitante.edad')->get();


        $totalE = DB::table('prorroga')->leftJoin('solicitante','prorroga.solicitante_id','=','solicitante.id')->select('solicitante.edad', DB::raw('COUNT(prorroga.solicitante_id) as extranjero'))->whereBetween('prorroga.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->count('prorroga.solicitante_id');

              $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Prorrogas turísticas de hijos de padres salvadoreños con pasaporte y padres extranjeros.";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

        }

      return view('tactico.pnt09')->with('hijosSalvador',$hijosSalvador)->with('extranjero',$extranjero)->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('totalH',$totalH)->with('totalE',$totalE);
            
  }
  public function pdfprorrogaPTNAHPSyPTNAHPE(Request $request){
    $fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){


       $hijosSalvador = DB::table('hps')->leftJoin('solicitante','hps.solicitante_id','=','solicitante.id')->select('solicitante.edad', DB::raw('COUNT(hps.solicitante_id) as hijo'))->whereBetween('hps.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->groupBy('solicitante.edad')->get();

       $totalH = DB::table('hps')->leftJoin('solicitante','hps.solicitante_id','=','solicitante.id')->whereBetween('hps.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->count('hps.solicitante_id');

        $extranjero = DB::table('prorroga')->leftJoin('solicitante','prorroga.solicitante_id','=','solicitante.id')->select('solicitante.edad', DB::raw('COUNT(prorroga.solicitante_id) as extranjero'))->whereBetween('prorroga.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->groupBy('solicitante.edad')->get();

        $totalE = DB::table('prorroga')->leftJoin('solicitante','prorroga.solicitante_id','=','solicitante.id')->select('solicitante.edad', DB::raw('COUNT(prorroga.solicitante_id) as extranjero'))->whereBetween('prorroga.fecha_servidor',[$fecha_inicio, $fecha_final])->whereBetween('solicitante.edad',[0,15])->count('prorroga.solicitante_id');


        $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Prorrogas turísticas de hijos de padres salvadoreños con pasaporte y padres extranjeros, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

        }

        $pdf = \PDF::loadView('tactico.rpt09', ['hijosSalvador'=>$hijosSalvador], compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'extranjero','totalH','totalE'))->setWarnings(false);
        return $pdf->stream('reportePT09.pdf');

  }
    
}
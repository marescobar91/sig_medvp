<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \PDF;
use App\Bitacora;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;
use Carbon\Carbon;
use App\VisasMultiple;


class VisasMultipleController extends Controller
{
    
    public function verVisaM(){
    	$vacio="";
    	return view('tactico.pnt10')->with('vacio', $vacio);
    }

    public function visaMultiple(Request $request){

    	$fecha_inicio = $request->input('fecha_inicio');
    	$fecha_final = $request->input('fecha_final');
    	$fechaI = new  \DateTime($fecha_inicio);
    	$iFechas = $fechaI->format('F');
    	$fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');

        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');

          if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}


         if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('VisasMultipleController@verVisaM');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('VisasMultipleController@verVisaM');
        }
         if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('VisasMultipleController@verVisaM');
        }

       $visasM = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visa_multiple'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.nombre')->get();

       $total = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->count('visa_multiple.solicitante_id');
         //dd($total);
        $vacio="Existe";

        $noVisaMul= $visasM->isEmpty();


         if($noVisaMul == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('VisasMultipleController@verVisaM');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Visas Multiples no Residentes";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

            }


		return view('tactico.pnt10')->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('visasM', $visasM)->with('total', $total);

		
    	
		
	}

	public function pdfVisasMultiple (Request $request){
		$fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');

        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

        if($fecha_inicio!=null && $fecha_final!=null){
        	$vacio="Existe";

        $visasM = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visa_multiple'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.nombre')->get();

       $total = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->count('visa_multiple.solicitante_id');

         $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Visas Multiples no Residentes, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
        }//fin if

        $pdf = \PDF::loadView('tactico.rpt10', ['visasM'=>$visasM], compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total'))->setWarnings(false);

        return $pdf->stream('reportePT10.pdf');

	}//fin pdf visa mul


	 public function verVisaMulInver(){
    	$vacio="";
    	return view('tactico.pnt11')->with('vacio', $vacio);
    }

    public function visaMultipleInversionista(Request $request){
    	$fecha_inicio = $request->input('fecha_inicio');
    	$fecha_final = $request->input('fecha_final');
    	$fechaI = new  \DateTime($fecha_inicio);
    	$iFechas = $fechaI->format('F');
    	$fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');

        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');

        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}


         if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('VisasMultipleController@verVisaMulInver');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('VisasMultipleController@verVisaMulInver');
        }
         if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('VisasMultipleController@verVisaMulInver');
        }

        $visasMulInver = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visa_multiple_inversionista'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Inversionista')->groupBy('ubicacion_geografica.nombre')->get();

       $total = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Inversionista')->count('visa_multiple.solicitante_id');
         //dd($total);
        $vacio="Existe";

        $noVisaMulInver= $visasMulInver->isEmpty();

         if($noVisaMulInver == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('VisasMultipleController@verVisaMulInver');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Visas Multiples Para Inversionistas";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

            }


		return view('tactico.pnt11')->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('visasMulInver', $visasMulInver)->with('total', $total);


    }

    public function pdfVisasMultipleInversionista(Request $request){

    	$fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');

        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');

        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

        if($fecha_inicio!=null && $fecha_final!=null){
        	$vacio="Existe";

         $visasMulInver = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_multiple.solicitante_id) as total_visa_multiple_inversionista'))->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('profesion.nombre_profesion', '=', 'Inversionista')->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.nombre')->get();

       $total = DB::table('visa_multiple')->leftJoin('solicitante', 'visa_multiple.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_multiple.estado_id', '=', 'estado.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_multiple.fecha_servidor', [$fecha_inicio, $fecha_final])->where('profesion.nombre_profesion', '=', 'Inversionista')->where('estado.id', '=', 1)->count('visa_multiple.solicitante_id');
        
       $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Visas Multiples Para Inversionistas, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();


        }//fin if

        $pdf = \PDF::loadView('tactico.rpt11', ['visasMulInver'=>$visasMulInver], compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total'))->setWarnings(false);

        return $pdf->stream('reportePT11.pdf');
    	
    }


}



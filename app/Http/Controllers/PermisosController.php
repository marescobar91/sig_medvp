<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\VisaSalida;
use App\Solicitante;
use App\UbicacionGeografica;
use \PDF;
use App\Bitacora;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class PermisosController extends Controller
{
    //
    public function verPerEspecialIngreso(){
    	$vacio = "";
    	return view('tactico.pnt06')->with('vacio',$vacio);
    }

    public function PerEspecialIngreso(Request $request){
    	$fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialIngreso');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialIngreso');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialIngreso');
        }

        $vacio="Existe";
        $permisos =  DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_salida.solicitante_id) as total_ingresos'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.nombre')->where('autorizado','=','Si')->get();
        
        $total = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('autorizado','=','Si')->count('visa_salida.solicitante_id');

        $noProrroga= $permisos->isEmpty();
                
        if($noProrroga == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('PermisosController@verPerEspecialIngreso');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Permisos Especial de Ingreso";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
            
		}
       		//dd($prorrogas);

      return view('tactico.pnt06')->with('permisos', $permisos)->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('total',$total);
     
    }

    public function pdfPerEspecialIngreso(Request $request){
        $fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

        $permisos =  DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_salida.solicitante_id) as total_ingresos'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->groupBy('ubicacion_geografica.nombre')->where('autorizado','=','Si')->get();
        
        $total = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('autorizado','=','Si')->count('visa_salida.solicitante_id');


            $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Permisos Especial de Ingreso, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

        }
        
        $pdf = \PDF::loadView('tactico.rpt06',  ['permisos'=>$permisos], compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total'))->setWarnings(false);
        return $pdf->stream('reportePT06.pdf');
        
    }
    public function verPerEspecialInver(){
        $vacio = "";
        return view('tactico.pnt07')->with('vacio',$vacio);
    }
    public function PerEspecialInver(Request $request){
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F'); 
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialInver');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialInver');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialInver');
        }

        $vacio="Existe";
        $permisos =  DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_salida.solicitante_id) as inversionista'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Inversionista')->groupBy('ubicacion_geografica.nombre')->get();
        
        $total = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Inversionista')->count('visa_salida.solicitante_id');

        $noProrroga= $permisos->isEmpty();
                
        if($noProrroga == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('PermisosController@verPerEspecialInver');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Permisos Especial de Ingreso Inversionista";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
            
        }
            //dd($prorrogas);

      return view('tactico.pnt07')->with('permisos', $permisos)->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('total',$total);
    }

    public function pdfPerEspecialInver(Request $request){
         $fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

       $permisos =  DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_salida.solicitante_id) as inversionista'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Inversionista')->groupBy('ubicacion_geografica.nombre')->get();
        
        $total = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Inversionista')->count('visa_salida.solicitante_id');

              $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Permisos Especial de Ingreso Inversionista, generando PDF";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
        }
        
        $pdf = \PDF::loadView('tactico.rpt07',  ['permisos'=>$permisos], compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total'))->setWarnings(false);
        return $pdf->stream('reportePT07.pdf');
    }

    public function verPerEspecialMarino(){
        $vacio = "";
        return view('tactico.pnt08')->with('vacio',$vacio);
    }

    public function perEspecialMarino(Request $request){
        $fecha_inicio = $request->input('fecha_inicio');
        $fecha_final = $request->input('fecha_final');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

        if($fecha_final < $fecha_inicio){

            flash("¡ERROR! ¡La fecha final no puede ser menor que la fecha de inicio!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialMarino');
        }
        if($fecha_inicio > $date){

             flash("¡ERROR! ¡La fecha inicio no puede ser mayor que la fecha de actual!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialMarino');
        }
        if($fecha_final > $date){

             flash("¡ERROR! ¡La fecha final no puede ser mayor que la fecha actual!")->error()->important();
            return redirect()->action('PermisosController@verPerEspecialMarino');
        }

        $vacio="Existe";
        $permisos =  DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_salida.solicitante_id) as marino'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Marinos')->groupBy('ubicacion_geografica.nombre')->get();
        
        $total = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Marinos')->count('visa_salida.solicitante_id');

        $noProrroga= $permisos->isEmpty();
                
        if($noProrroga == true)
                {
                flash('¡No se tiene dicha información en el sistema!')->error()->important();
                return redirect()->action('PermisosController@verPerEspecialMarino');
                }

                $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Permisos Especial para Marinos";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();
            
        }
            //dd($prorrogas);

      return view('tactico.pnt08')->with('permisos', $permisos)->with('vacio', $vacio)->with('finicio', $fecha_inicio)->with('ffinal', $fecha_final)->with('ife', $ife)->with('fe', $fe)->with('total',$total);
    }

    public function pdfPerEspecialMarino(Request $request){
         $fecha_inicio = $request->input('fechaini');
        $fecha_final = $request->input('fechafin');
        $fechaI= new \DateTime($fecha_inicio);
        $iFechas = $fechaI->format('F');
        $fechaF = new \DateTime($fecha_final);
        $fFechas = $fechaF->format('F');
        Carbon::setLocale('es_SV');
        $fecha = Carbon::now(new \DateTimeZone('America/El_Salvador'));
        $hora= $fecha->toTimeString(); 
        $date = $fecha->format('Y-m-d');
    
        if($iFechas=='January')      {$ife='Enero';}
        if($iFechas=='February')     {$ife='Febrero';}
        if($iFechas=='March')        {$ife='Marzo';}
        if($iFechas=='April')        {$ife='Abril';}
        if($iFechas=='May')          {$ife='Mayo';}
        if($iFechas=='June')         {$ife='Junio';}
        if($iFechas=='July')         {$ife='Julio';}
        if($iFechas=='August')       {$ife='Agosto';}
        if($iFechas=='September')    {$ife='Septiembre';}
        if($iFechas=='October')      {$ife='Octubre';}
        if($iFechas=='November')     {$ife='Noviembre';}
        if($iFechas=='December')     {$ife='Diciembre';}
        
        if($fFechas=='January')      {$fe='Enero';}
        if($fFechas=='February')     {$fe='Febrero';}
        if($fFechas=='March')        {$fe='Marzo';}
        if($fFechas=='April')        {$fe='Abril';}
        if($fFechas=='May')          {$fe='Mayo';}
        if($fFechas=='June')         {$fe='Junio';}
        if($fFechas=='July')         {$fe='Julio';}
        if($fFechas=='August')       {$fe='Agosto';}
        if($fFechas=='September')    {$fe='Septiembre';}
        if($fFechas=='October')      {$fe='Octubre';}
        if($fFechas=='November')     {$fe='Noviembre';}
        if($fFechas=='December')     {$fe='Diciembre';}

       if($fecha_inicio!=null && $fecha_final!=null){

       $permisos =  DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->select('ubicacion_geografica.nombre', DB::raw('COUNT(visa_salida.solicitante_id) as marino'))->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Marinos')->groupBy('ubicacion_geografica.nombre')->get();
        
        $total = DB::table('visa_salida')->leftJoin('solicitante', 'visa_salida.solicitante_id', '=', 'solicitante.id')->leftJoin('profesion', 'profesion.id', '=', 'solicitante.profesion_id')->leftJoin('estado', 'visa_salida.estado_id', '=', 'estado.id')->leftJoin('ubicacion_geografica', 'solicitante.ubicacion_geografica_id', '=', 'ubicacion_geografica.id')->whereBetween('visa_salida.fecha_servidor', [$fecha_inicio, $fecha_final])->where('estado.id', '=', 1)->where('profesion.nombre_profesion', '=', 'Marinos')->count('visa_salida.solicitante_id');

             $bitacora = new Bitacora();
                $id = Auth::id();
                $bitacora->user_id = $id;
                $bitacora->nombre_reporte = "Reporte de Permisos Especial para Marinos";
                $bitacora->fecha_consulta = $fecha;
                $bitacora->hora_consulta = $hora;
                $bitacora->save();

        }
        
        $pdf = \PDF::loadView('tactico.rpt08',  ['permisos'=>$permisos], compact('fecha_inicio', 'ife', 'fecha_final', 'fe', 'total'))->setWarnings(false);
        return $pdf->stream('reportePT08.pdf');
    }

}


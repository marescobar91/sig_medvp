<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    //

    protected $table="estado"; 

   
    protected $fillable =['estado', 'abreviatura'];
    

    public $timestamps=true;

    public function hps(){

    	return $this->hasMany('App\Hps');
    }

    public function prorroga(){

    	return $this->hasMany('App\Prorroga');
    }
    public function visaconsul(){

    	return $this->hasMany('App\VisaConsul');
    }

    public function visamultiple(){

    	return $this->hasMany('App\VisaMultiple');
    }

    public function visasalida(){

    	return $this->hasMany('App\VisaSalida');
    }
}

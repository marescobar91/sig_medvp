<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pariente extends Model
{
    //
     protected $table="pariente"; 

    protected $fillable =['hps_id', 'prorroga_id', 'ubicacion_geografica_id', 'visa_multiple_id', 'nombre_pariente', 'nacimiento_pariente', 'parentesco'];


    public function hps(){

    	return $this->belongsTo('App\Hps', 'hps_id');
    	
    }

    public function prorroga(){

    	return $this->belongsTo('App\Prorroga', 'prorroga_id');
    }

    public function ubicacion(){

    	return $this->belongsTo('App\UbicacionGeografica', 'ubicacion_geografica_id');
    }

    public function visamultiple(){

    	return $this->belongsTo('App\VisaMultiple', 'visa_multiple_id');
    }
}

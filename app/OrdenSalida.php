<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenSalida extends Model
{
    protected $table="orden_salida"; 

    protected $fillable =['solicitante_id', 'ubicacion_geografica_id', 'fecha_entrada', 'motivo', 'infraccion', 'base_legal', 'via_salida', 'autorizado', 'fecha_desde', 'fecha_hasta', 'fecha_servidor'];


    public function solicitante(){

    	return $this->belongsTo('App\Solicitante', 'solicitante_id');

    }

    public function ubicacion(){
    	return $this->belongsTo('App\UbicacionGeografica', 'ubicacion_geografica_id');
    }
}

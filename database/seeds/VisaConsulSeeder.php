<?php

use Illuminate\Database\Seeder;
use App\VisaConsul;

class VisaConsulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(VisaConsul::class, 260)-> create();
    }
}

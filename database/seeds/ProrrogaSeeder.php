<?php

use Illuminate\Database\Seeder;
use App\Prorroga;


class ProrrogaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Prorroga::class, 400)-> create();
    }
}

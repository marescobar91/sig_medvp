<?php

use Illuminate\Database\Seeder;
use App\TipoDocumento;

class DocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $documento = [

        	[
        		'tipo_documento'		=> 	'Cedula de Identidad'
        	],
        	[
        		'tipo_documento'		=> 	'Carnet'
        	],
        	[
        		'tipo_documento'		=> 	'Partida Nacimiento'
        	],
        	[
        		'tipo_documento'		=> 	'DUI'
        	],
        	[
        		'tipo_documento'		=> 	'NIT'
        	],
        	[
        		'tipo_documento'		=> 	'Carnet Minoridad'
        	],

        ];

        foreach ($documento as $key => $value) {
            TipoDocumento::create($value);
        }
    }
}

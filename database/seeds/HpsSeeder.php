<?php

use Illuminate\Database\Seeder;
use App\Hps;

class HpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Hps::class, 470)-> create();
    }

    
}

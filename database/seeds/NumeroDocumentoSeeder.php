<?php

use Illuminate\Database\Seeder;
use App\NumeroDocumento;

class NumeroDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(NumeroDocumento::class, 400)-> create();
    }
}

<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	 $this->call(PermissionSeeder::class);
    	 $this->call(DocumentoSeeder::class);
    	 $this->call(EstadoSeeder::class);
    	 $this->call(PasaporteSeeder::class);
    	 $this->call(ProfesionSeeder::class);
    	 $this->call(UbicacionSeeder::class);
         $this->call(SolicitanteSeeder::class);
         $this->call(ProrrogaSeeder::class);
         $this->call(NumeroDocumentoSeeder::class);
         $this->call(NumeroPasaporteSeeder::class);
         $this->call(OrdenSalidaSeeder::class);
         $this->call(VisaConsulSeeder::class);
         $this->call(VisaMultipleSeeder::class);
         $this->call(VisaSalidaSeeder::class);
         $this->call(HpsSeeder::class);
         $this->call(ParienteSeeder::class);     
    }
}

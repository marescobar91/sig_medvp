<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $permission = [
            [
                'slug' => 'user-list',
                'name' => 'Listar Usuarios',
                'description' => 'Lista y Navega todos los usuarios del sistema'
            ],
            [
                'slug' => 'user-edit',
                'name' => 'Editar Usuario',
                'description' => 'Edición de usuarios del sistema'
            ],
            [
                'slug' => 'user-delete',
                'name' => 'Eliminar Usuario',
                'description' => 'Elimina los usuarios del sistema'
            ],

              [
                'slug' => 'roles-create',
                'name' => 'Crear Roles',
                'description' => 'Crear roles del sistema'
            ],

             [
                'slug' => 'user-inactivo',
                'name' => 'Usuario Inactivo',
                'description' => 'Lista y navega los usuarios inactivos'
            ],

              [
                'slug' => 'user-habilitar',
                'name' => 'Habilitar Usuario',
                'description' => 'Hbailita los usuarios inactivos del sistema'
            ],

              [
                'slug' => 'admin-passowrd',
                'name' => 'Cambio de Contraseña',
                'description' => 'Cambio de Contraseña por el administrador'
            ],
             [
                'slug' => 'menu-estrategico',
                'name' => 'Menu estrategico',
                'description' => 'Muestra el menu del usuario estrategico'
            ],

             [
                'slug' => 'menu-tactico',
                'name' => 'Menu Táctico',
                'description' => 'Muestra el menu del usuario tactico'
            ],

             [
                'slug' => 'menu-administrador',
                'name' => 'Menu Administrador ',
                'description' => 'Muestra el menu del usuario administrador'
            ],

              [
                'slug' => 'reporte-estrategico',
                'name' => 'Reportes Estrategicos',
                'description' => 'Muestra el reporte estrategico y tacticos al usuario estrategico'
            ],
            [
                'slug' => 'reporte-tactico',
                'name' => 'Reportes Tactico',
                'description' => 'Muestra el reporte tacticos al usuario tactico'
            ],

             

            
        ];
        foreach ($permission as $key => $value) {
            Permission::create($value);
        }


    }
}

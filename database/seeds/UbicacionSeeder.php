<?php

use Illuminate\Database\Seeder;
use App\UbicacionGeografica;
class UbicacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ubicacion = [
            [
                'nombre' 				=> 	'El Salvador',
                'abreviatura'			=> 	'SV',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Nicaragua',
                'abreviatura'			=> 	'NI',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Guatemala',
                'abreviatura'			=> 	'GT',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Honduras',
                'abreviatura'			=> 	'HN',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],
             [
                'nombre' 				=> 	'Costa Rica',
                'abreviatura'			=> 	'CR',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],
             [
                'nombre' 				=> 	'Panamá',
                'abreviatura'			=> 	'PAN',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],
             [
                'nombre' 				=> 	'Perú',
                'abreviatura'			=> 	'PE',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Argentina',
                'abreviatura'			=> 	'ARG',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Bolivia',
                'abreviatura'			=> 	'BO',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

              [
                'nombre' 				=> 	'Brasil',
                'abreviatura'			=> 	'BR',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

              [
                'nombre' 				=> 	'Chile',
                'abreviatura'			=> 	'CL',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Ecuador',
                'abreviatura'			=> 	'EC',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

              [
                'nombre' 				=> 	'Guyana',
                'abreviatura'			=> 	'GY',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Paraguay',
                'abreviatura'			=> 	'PAR',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],
             
               [
                'nombre' 				=> 	'Uruguay',
                'abreviatura'			=> 	'UY',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

              [
                'nombre' 				=> 	'Surinam',
                'abreviatura'			=> 	'SUR',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Venezuela',
                'abreviatura'			=> 	'VEN',
                'abreviatura_lata' 		=>	'SA',
                'nivel'					=>	'1'
            ],

            [
                'nombre' 				=> 	'Bélice',
                'abreviatura'			=> 	'BE',
                'abreviatura_lata' 		=>	'CA',
                'nivel'					=>	'1'
            ],

             [
                'nombre' 				=> 	'Canadá',
                'abreviatura'			=> 	'CA',
                'abreviatura_lata' 		=>	'NA',
                'nivel'					=>	'1'
            ],

              [
                'nombre' 				=> 	'Estados Unidos',
                'abreviatura'			=> 	'USA',
                'abreviatura_lata' 		=>	'NA',
                'nivel'					=>	'1'
            ],

            	[
                'nombre' 				=> 	'México',
                'abreviatura'			=> 	'MEX',
                'abreviatura_lata' 		=>	'NA',
                'nivel'					=>	'1'
            ],

        ];
        foreach ($ubicacion as $key => $value) {
            UbicacionGeografica::create($value);
        }
    }
}

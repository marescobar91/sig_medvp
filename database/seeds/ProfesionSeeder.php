<?php

use Illuminate\Database\Seeder;
use App\Profesion;

class ProfesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $profesion = [

        	[
        		'nombre_profesion'	=>	'Marinos'
        	],

        	[
        		'nombre_profesion'	=>	'Abogado'
        	],
        	[
        		'nombre_profesion'	=>	'Profesor'
        	],

        	[
        		'nombre_profesion'	=>	'Ministro'
        	],
        	[
        		'nombre_profesion'	=>	'Presidente'
        	],
        	[
        		'nombre_profesion'	=>	'Juez'
        	],
        	[
        		'nombre_profesion'	=>	'Actor/Actris'
        	],
        	[
        		'nombre_profesion'	=>	'Médico'
        	],

        	[
        		'nombre_profesion'	=>	'Físico/Física'
        	],

        	[
        		'nombre_profesion'	=>	'Ingeniero'
        	],

        	[
        		'nombre_profesion'	=>	'Economista'
        	],

        	[
        		'nombre_profesion'	=>	'Inversionista'
        	],
        	[
        		'nombre_profesion'	=>	'Licenciado/a'
        	],
            [
                'nombre_profesion'  =>  'Consul'
            ],

        ];
         foreach ($profesion as $key => $value) {
            Profesion::create($value);
        }
    }
}

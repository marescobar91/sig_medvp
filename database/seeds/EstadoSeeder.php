<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $estado = [

        	[
        		'estado'		=>	'Aprobado',
        		'abreviatura'	=>	'A'
        	],

        	[
        		'estado'		=>	'No Aprobado',
        		'abreviatura'	=>	'NoA'
        	],
        ];

         foreach ($estado as $key => $value) {
            Estado::create($value);
        }
    }
}

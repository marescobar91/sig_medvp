<?php

use Illuminate\Database\Seeder;
use App\Pariente;

class ParienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Pariente::class, 460)-> create();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\VisaSalida;

class VisaSalidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(VisaSalida::class, 400)-> create();
    }
}

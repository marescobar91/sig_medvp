<?php

use Illuminate\Database\Seeder;
use App\VisaMultiple;

class VisaMultipleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(VisaMultiple::class, 320)-> create();
    }
}

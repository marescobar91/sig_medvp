<?php

use Illuminate\Database\Seeder;
use App\NumeroPasaporte;

class NumeroPasaporteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(NumeroPasaporte::class, 460)-> create();
    }
}

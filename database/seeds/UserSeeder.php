<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = ['name' => 'super admin', 'slug' => 'SuperAdmin', 'description' => 'Todos los Permisos', 'special'=> 'all-access'];

        //especial otorga todos los permiso al rol.
        $role = Role::create($role);
    
      
        //3) Creamos el super usuario
        $user = ['nombre'=> 'Sofia', 'apellido'=>'Marroquin','username' => 'Super Usuario', 'email' => 'superuser@gmail.com', 'password' => Hash::make('admin12345')];
        $user = User::create($user);
        //4) Set User Role
        $user->roles()->sync($role);
    }
}

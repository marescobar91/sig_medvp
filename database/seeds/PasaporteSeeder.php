<?php

use Illuminate\Database\Seeder;
use App\TipoPasaporte;

class PasaporteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $pasaporte = [

        	[
        		'tipopasaporte'		=>	'Pasaporte Diplomático'
        	],
        	[
        		'tipopasaporte'		=>	'Pasaporte Oficial'
        	],
        	[
        		'tipopasaporte'		=>	'Oficial de Acordeón'
        	],
        	[
        		'tipopasaporte'		=>	'Especial de Libreta'
        	],
        	[
        		'tipopasaporte'		=>	'Especial de Acordeón'
        	],
        	[
        		'tipopasaporte'		=>	'Salvoconductos'
        	],
        	[
        		'tipopasaporte'		=>	'Permisos Especiales'
        	],


        ];

        foreach ($pasaporte as $key => $value) {
            TipoPasaporte::create($value);
        }
    }
}


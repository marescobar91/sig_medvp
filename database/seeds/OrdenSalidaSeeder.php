<?php

use Illuminate\Database\Seeder;
use App\OrdenSalida;

class OrdenSalidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(OrdenSalida::class, 460)-> create();
    }
}

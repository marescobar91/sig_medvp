<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableVisaConsul extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_consul', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitante_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->date('f_entradaca4')->nullable();
            $table->date('fecha_salida')->nullable();
            $table->string('proposito', 100)->nullable();
            $table->string('autorizado', 25)->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();
            $table->date('fecha_servidor')->nullable();

            $table->foreign('solicitante_id')->references('id')->on('solicitante')->onDelete('cascade');

            $table->foreign('estado_id')->references('id')->on('estado')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_consul');
    }
}

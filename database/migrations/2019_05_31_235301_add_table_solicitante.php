<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSolicitante extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitante', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ubicacion_geografica_id')->unsigned();
            $table->integer('profesion_id')->unsigned();
            $table->enum('genero', ['F', 'M'])->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->integer('edad')->nullable();
            $table->string('tramite', 25)->nullable();
            $table->date('fecha_movimiento')->nullable();
            $table->date('fecha_servidor')->nullable();
            $table->foreign('ubicacion_geografica_id')->references('id')->on('ubicacion_geografica')->onDelete('cascade');
            $table->foreign('profesion_id')->references('id')->on('profesion')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitante');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableProrroga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prorroga', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitante_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->date('fecha_ingreso')->nullable();
            $table->date('fecha_residencia')->nullable();
            $table->string('trabajo', 50)->nullable();
            $table->string('actividad_realizar', 50)->nullable();
            $table->string('oferta_trabajo', 50)->nullable();
            $table->string('prov_capital', 50)->nullable();
            $table->string('autorizado', 25)->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();
            $table->date('fecha_servidor')->nullable();

            $table->foreign('estado_id')->references('id')->on('estado')->onDelete('cascade');

            $table->foreign('solicitante_id')->references('id')->on('solicitante')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prorroga');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableNumeropasaporte extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numeropasaporte', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasaporte_id')->unsigned();
            $table->integer('solicitante_id')->unsigned();
            $table->char('numeropasaporte', 20)->nullable();
            $table->foreign('pasaporte_id')->references('id')->on('tipopasaporte')->onDelete('cascade');
            $table->foreign('solicitante_id')->references('id')->on('solicitante')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numeropasaporte');
    }
}

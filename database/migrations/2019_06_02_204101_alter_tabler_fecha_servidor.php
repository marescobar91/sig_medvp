<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablerFechaServidor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orden_salida', function (Blueprint $table) {
            //
            $table->date('fecha_servidor')->after('fecha_hasta')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orden_salida', function (Blueprint $table) {
            //

            $table->dropColumn('fecha_servidor');
        });
    }
}

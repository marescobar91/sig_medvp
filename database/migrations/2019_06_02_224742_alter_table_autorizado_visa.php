
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableAutorizadoVisa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visa_salida', function (Blueprint $table) {
            //
            $table->string('autorizado', 25)->after('cde_respaldo')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visa_salida', function (Blueprint $table) {
            //
            $table->dropColumn('autorizado');
        });
    }
}

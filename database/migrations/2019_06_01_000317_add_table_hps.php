<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableHps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitante_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->date('fecha_entrada')->nullable();
            $table->string('motivo', 100)->nullable();
            $table->string('autorizado', 25)->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();
            $table->text('observacion')->nullable();
            $table->date('fecha_servidor')->nullable();
            $table->foreign('solicitante_id')->references('id')->on('solicitante')->onDelete('cascade');
            $table->foreign('estado_id')->references('id')->on('estado')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hps');
    }
}

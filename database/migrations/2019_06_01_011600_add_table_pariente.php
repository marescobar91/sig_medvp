<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePariente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pariente', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hps_id')->unsigned()->nullable();
            $table->integer('prorroga_id')->unsigned()->nullable();
            $table->integer('ubicacion_geografica_id')->unsigned()->nullable();
            $table->integer('visa_multiple_id')->unsigned()->nullable();
            $table->string('nombre_pariente', 100)->nullable();
            $table->date('nacimiento_pariente')->nullable();
            $table->string('parentesco', 25)->nullable();

            $table->foreign('ubicacion_geografica_id')->references('id')->on('ubicacion_geografica')->onDelete('cascade');

            $table->foreign('hps_id')->references('id')->on('hps')->onDelete('cascade');

            $table->foreign('prorroga_id')->references('id')->on('prorroga')->onDelete('cascade');
            $table->timestamps();

            $table->foreign('visa_multiple_id')->references('id')->on('visa_multiple')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pariente');
    }
}

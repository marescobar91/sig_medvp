<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableHpsFechasalida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hps', function (Blueprint $table) {
            //
            $table->date('fecha_salida')->after('fecha_entrada')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hps', function (Blueprint $table) {
            //
            $table->dropColumn('fecha_salida');
        });
    }
}

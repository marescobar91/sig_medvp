<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableOrdenSalida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_salida', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitante_id')->unsigned();
            $table->integer('ubicacion_geografica_id')->unsigned();
            $table->date('fecha_entrada')->nullable();
            $table->string('motivo',100)->nullable();
            $table->string('infraccion', 50)->nullable();
            $table->text('base_legal')->nullable();
            $table->string('via_salida', 25)->nullable();
            $table->string('autorizado', 25)->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();

            $table->foreign('ubicacion_geografica_id')->references('id')->on('ubicacion_geografica')->onDelete('cascade');
            $table->foreign('solicitante_id')->references('id')->on('solicitante')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_salida');
    }
}

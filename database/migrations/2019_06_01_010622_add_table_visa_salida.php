<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableVisaSalida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_salida', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('solicitante_id')->unsigned();
            $table->integer('ubicacion_geografica_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->date('fecha_entrada')->nullable();
            $table->date('fecha_salida')->nullable();
            $table->string('via_salida', 25)->nullable();
            $table->string('proposito', 100)->nullable();
            $table->string('motivo', 100)->nullable();
            $table->string('cde_respaldo', 20)->nullable();
            $table->date('fecha_desde')->nullable();
            $table->date('fecha_hasta')->nullable();
            $table->text('observacion')->nullable();
            $table->date('fecha_servidor')->nullable();

            $table->foreign('ubicacion_geografica_id')->references('id')->on('ubicacion_geografica')->onDelete('cascade');
            $table->foreign('estado_id')->references('id')->on('estado')->onDelete('cascade');
            $table->foreign('solicitante_id')->references('id')->on('solicitante')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_salida');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
 $factory->define(App\Solicitante::class, function(Faker\Generator $faker) {
    	return [
        	'ubicacion_geografica_id'		=> App\UbicacionGeografica::inRandomOrder()->value('id')?: factory(App\UbicacionGeografica::class),
        	'profesion_id'					=> App\Profesion::inRandomOrder()->value('id')?: factory(App\Profesion::class),
        	'genero'							=> $faker->randomElement(['F', 'M']),
        	'fecha_nacimiento'				=> $faker->date($format = 'Y-m-d', $max = 'now'),
        	'edad'							=> $faker->numberBetween($min = 0, $max = 50),	
        	'fecha_movimiento'				=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
        	'fecha_servidor'				=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)
        	];
});

 $factory->define(App\Prorroga::class, function(Faker\Generator $faker){
 	return [

 		'solicitante_id'		=> App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 		'estado_id'				=> App\Estado::inRandomOrder()->value('id')?: factory(App\Estado::class),
 		'fecha_ingreso'			=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
 		'fecha_residencia'		=> $faker->dateTimeBetween($startDate = 'fecha_ingreso', $endDate = '90 days', $timezone = null),
 		'trabajo'				=> $faker->jobTitle,
 		'actividad_realizar'	=> $faker->sentence($nbWords = 2, $variableNbWords = true),
 		'oferta_trabajo'		=> $faker->randomElement(['Si', 'No']),
 		'autorizado'			=> $faker->randomElement(['Si', 'No']),
 		'fecha_servidor'		=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)		

 	];
 });


 $factory->define(App\Hps::class, function(Faker\Generator $faker){
 	return [
 			'solicitante_id'	=>	App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 			'estado_id'			=>	App\Estado::inRandomOrder()->value('id')?: factory(App\Estado::class),
 			'fecha_entrada'		=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
 			'fecha_salida'		=> $faker->dateTimeBetween($startDate = 'fecha_entrada', $endDate = '90 days', $timezone = null),
 			'motivo'			=> $faker->sentence($nbWords = 2, $variableNbWords = true),	
 			'observacion'		=> $faker->text($maxNbChars = 100),   	
 			'fecha_servidor' 	=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)	
 	];
 });

 $factory->define(App\NumeroDocumento::class, function(Faker\Generator $faker){
 	return [

 			'documento_id'		=> App\TipoDocumento::inRandomOrder()->value('id')?: factory(App\TipoDocumento::class),
 			'solicitante_id'	=> App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 			'num_documento'		=> $faker->creditCardNumber
 		];

 });

 $factory->define(App\NumeroPasaporte::class, function(Faker\Generator $faker){
 	return [


 			'pasaporte_id'			=> App\TipoPasaporte::inRandomOrder()->value('id')?: factory(App\TipoPasaporte::class),
 			'solicitante_id'		=> App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 			'numeropasaporte'		=> $faker->creditCardNumber
 		];

 });

 $factory->define(App\OrdenSalida::class, function(Faker\Generator $faker){

 	return [
 			'solicitante_id'			=> App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 			'ubicacion_geografica_id'	=> App\UbicacionGeografica::inRandomOrder()->value('id')?: factory(App\UbicacionGeografica::class),
 			'fecha_entrada'				=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
 			'motivo'					=> $faker->sentence($nbWords = 6, $variableNbWords = true),
 			'infraccion'				=> $faker->sentence($nbWords = 2, $variableNbWords = true),
 			'base_legal'				=> $faker->text($maxNbChars = 100),
 			'via_salida'				=> $faker->word,
 			'autorizado'				=> $faker->randomElement(['Si', 'No']),	
 		    'fecha_servidor'			=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)
 	];
 });

 $factory->define(App\VisaConsul::class, function(Faker\Generator $faker){

 	return [
 		'solicitante_id'		=> App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 		'estado_id'				=> App\Estado::inRandomOrder()->value('id')?: factory(App\Estado::class),
 		'f_entradaca4'			=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
 		'fecha_salida'			=> $faker->dateTimeBetween($startDate = 'f_entradaca4', $endDate = '90 days', $timezone = null),
 		'proposito'				=> $faker->text($maxNbChars=100),
 		'autorizado'			=> $faker->randomElement(['Si', 'No']),
 		'fecha_servidor'		=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)
 	];
 });

 $factory->define(App\VisaMultiple::class, function(Faker\Generator $faker){
 	return [
 		'solicitante_id'			=> App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 		'estado_id'					=> App\Estado::inRandomOrder()->value('id')?: factory(App\Estado::class),
 		'fecha_ingreso'				=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
 		'fecha_residencia'			=>$faker->dateTimeBetween($startDate = 'fecha_ingreso', $endDate = '20 days', $timezone = null),
 		'cal_migratoria'			=> $faker->word,
 		'actividad_realizada'		=> $faker->word,
 		'autorizado'				=> $faker->randomElement(['Si', 'No']),
 		'dictamen'				=> $faker->text($maxNbChars=100),
 		'fecha_servidor'			=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)	
 		];
 });

 $factory->define(App\VisaSalida::class, function(Faker\Generator $faker){

 		return [
 			'solicitante_id'			=>	App\Solicitante::inRandomOrder()->value('id')?: factory(App\Solicitante::class),
 			'ubicacion_geografica_id'	=> App\UbicacionGeografica::inRandomOrder()->value('id')?: factory(App\UbicacionGeografica::class),
 			'estado_id'					=> App\Estado::inRandomOrder()->value('id')?: factory(App\Estado::class),
 			'fecha_entrada'				=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
 			'fecha_salida'				=> $faker->dateTimeBetween($startDate = 'fecha_entrada', $endDate = '15 days', $timezone = null),
 			'via_salida'				=> $faker->word,
 			'proposito'					=> $faker->text($maxNbChars=100),
 			'motivo'					=> $faker->text($maxNbChars=100),
 			'cde_respaldo'				=> $faker->word,
 			'autorizado'				=> $faker->randomElement(['Si', 'No']),	
 			'observacion'				=> $faker->text($maxNbChars=100),
 			'fecha_servidor'			=> $faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null)
 		];
 });

 $factory->define(App\Pariente::class, function(Faker\Generator $faker){
 	return[
 			'hps_id'						=> App\Hps::inRandomOrder()->value('id')?: factory(App\Hps::class),
 			'prorroga_id'					=> App\Prorroga::inRandomOrder()->value('id')?: factory(App\Prorroga::class),
 			'ubicacion_geografica_id'		=> App\UbicacionGeografica::inRandomOrder()->value('id')?: factory(App\UbicacionGeografica::class),	
 			'visa_multiple_id'				=> App\VisaMultiple::inRandomOrder()->value('id')?: factory(App\VisaMultiple::class),
 			'nombre_pariente'				=> $faker->name,
 			'nacimiento_pariente'			=> $faker->dateTimeBetween($startDate = '-20 years', $endDate = 'now', $timezone = null),
 			'parentesco'					=> $faker->word
 	];

 });

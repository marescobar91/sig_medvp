<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');   // view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){

	Route::resource('user', 'UsersController');
	Route::get('user/{id}/destroy', [
		'uses'	=>	'UsersController@destroy',
		'as'	=>	'user.destroy'
	]);

	Route::resource('roles', 'RolesController');
	Route::get('roles/{id}/destroy', [
		'uses'	=>	'RolesController@destroy',
		'as'	=>	'roles.destroy'
	]);

	//USUARIOS INACTIVO
//Administrador de la Aplicacion

//muesta los usuarios eliminados
	Route::get('/deshabilitado', [
		'uses'	=>	'UsersController@inactivo',
		'as'	=>	'user.inactivo'
	]);

	Route::get('{id}/habilitar',[
		'uses'	=> 'UsersController@habilitar',
		'as'	=>	'user.habilitar'
	]);

//Cambio de contraseña por parte del administrador
	Route::get('/{id}/password', [
		'uses'	=>	'UsersController@password',
		'as'	=>	'admin.password']);
	Route::post('/{id}/change/', [
        'uses'	=>	'UsersController@changepassword',
        'as'	=>	'admin.changepassword']);

//Cambiar de contraseña por parte del usuario
	Route::get('/changepassword/', [
		'uses'	=> 'UsersController@passwordchange',
		'as' 	=>	'user.password'
	]);
//guarda el cambio de contraseña
	Route::post('/updatepassword', [
		'uses'	=>	'UsersController@updatepassword',
		'as'	=>	'user.updatepassword'
	]);	

	Route::get('/seguridad', [
		'uses'	=>	'UsersController@seguridad',
		'as'	=>	'user.seguridad'
	]);

	Route::get('/bitacoras', [
		'uses'	=>	'BitacorasController@index',
		'as'	=>	'bitacoras.index'
	]);


	//Rutas asignados a los reportes que serán vistos
	//por el usuario estrategico

	Route::get('/prorrogas', [
		'uses'	=>	'ProrrogaController@ver',
		'as'	=>	'prorroga.ver'
	]);

	Route::post('prorrogas/filtro', [
		'uses'	=>	'ProrrogaController@prorroga',
		'as'	=>	'prorroga.filtro'
	]);

	Route::post('prorrogas/pdf', [
		'uses'	=>	'ProrrogaController@pdfProrroga',
		'as'	=>	'prorroga.pdfprorroga'
	]);

	Route::get('/visas', [
		'uses'	=>	'VisasController@ver',
		'as'	=>	'visas.ver'
	]);

	Route::post('visas/filtro', [
		'uses'	=>	'VisasController@visas',
		'as'	=>	'visas.filtro'

	]);

	Route::post('visas/pdfVisas', [
		'uses'	=>	'VisasController@pdfVisas',
		'as'	=>	'visas.pdfvisas'

	]);

	Route::get('/estadistica', [
		'uses'	=>	'ProrrogaController@verProrroga',
		'as'	=>	'prorroga.verprorroga'
	]);

	Route::post('estadistica/prorroga', [
		'uses'	=>	'ProrrogaController@estadisticaProrrogas',
		'as'	=>	'prorroga.estadistica'
	]);

	Route::post('estadistica/pdf', [
		'uses'	=>	'ProrrogaController@pdfEstadistica',
		'as'	=>	'prorroga.pdfestadistica'
	]);


	Route::get('/tramites', [
		'uses'	=>	'TramitesController@estadisticas',
		'as'	=>	'tramites.estadistica'
	]);

	Route::post('tramites/total', [
		'uses'	=>	'TramitesController@estadisticaTotal',
		'as'	=>	'tramites.total'
	]);

	Route::post('tramites/pdf', [
		'uses'	=> 	'TramitesController@tramitesPdf',
		'as'	=>	'tramites.pdf'
	]);

	Route::get('/estadisticas', [
		'uses'	=>	'VisasController@verEstadisticas',
		'as'	=>	'visas.vervisa'
	]);

	Route::post('estadistica/visas', [
		'uses'	=>	'VisasController@estadisticasVisas',
		'as'	=>	'visas.estadisticas'
	]);

	Route::post('estadisticas/pdf', [
		'uses'	=>	'VisasController@estadisticaPdf',
		'as'	=>	'visas.pdfestadistica'
	]);


//menus tacticos
	Route::get('visa/multiple/ver', [
		'uses'	=>	'VisasMultipleController@verVisaM',
		'as'	=>	'visas.multiple.ver'
	]);


	Route::post('multiple/visas/filtro',[
	'uses'=>'VisasMultipleController@visaMultiple',
	'as' => 'visas.multiple.no.residente'
	]);


	Route::get('ProrrogaHijo/ver',[
		'uses'  =>  'ProrrogaController@verProrrogaPTNAHPSyPTNAHPE',
		'as' 	=>	'prorroga.verPTNAHPSyPTNAHPE'
	]);


	Route::post('ProrrogaHijo/filtro',[
		'uses' 	=>	'ProrrogaController@prorrogaPTNAHPSyPTNAHPE',
		'as'	=>	'prorroga.PTNAHPSyPTNAHPE'
	]);

	Route::post('ProrrogaHijo/pdf',[
		'uses'	=>	'ProrrogaController@pdfprorrogaPTNAHPSyPTNAHPE',
		'as'	=>	'prorroga.pdfprorrogaPTNAHPSyPTNAHPE'
	]);

	Route::get('Especial/ingreso',[
		'uses'	=>	'PermisosController@verPerEspecialIngreso',
		'as'	=>	'permiso.verEspecialIngreso'
	]);

	Route::post('Especial/filtro',[
		'uses'	=>	'PermisosController@PerEspecialIngreso',
		'as'	=>	'permiso.especialIngreso'
	]);

	Route::post('Especial/pdf',[
		'uses'	=>	'PermisosController@pdfPerEspecialIngreso',
		'as'	=>	'permiso.pdf'
	]);

	Route::post('visa/multiple/pdf', [
		'uses'	=>	'VisasMultipleController@pdfVisasMultiple',
		'as'	=>	'visas.pdf.no.residente'
	]);


	Route::get('visa/multiple/inversionista/ver', [
		'uses'	=>	'VisasMultipleController@verVisaMulInver',
		'as'	=>	'visas.multiple.inversionista.ver'
	]);


	Route::post('multiple/visas/inver/filtro',[
	'uses'	=>	'VisasMultipleController@visaMultipleInversionista',
	'as' 	=> 	'visas.multiple.inversionista'
	]);

	Route::post('visa/multiple/inver/pdf', [
		'uses'	=>	'VisasMultipleController@pdfVisasMultipleInversionista',
		'as'	=>	'visas.mult.inversionista.pdf'
	]);

	Route::get('Especial/inversionista',[
		'uses'	=>	'PermisosController@verPerEspecialInver',
		'as' 	=>	'permisos.verInversionista'
	]);

	Route::post('Especial/filtro/inver',[
		'uses'	=>	'PermisosController@PerEspecialInver',
		'as'	=>	'permisos.inversion'
	]);

	Route::post('Especial/pdf/inver',[
		'uses'	=>	'PermisosController@pdfPerEspecialInver',
		'as'	=>	'permisos.inver.pdf'
	]);

	Route::get('Especial/marinos',[
		'uses'	=>	'PermisosController@verPerEspecialMarino',
		'as' 	=>	'permisos.marinos'
	]);

	Route::post('Especial/filtro/marino',[
		'uses'	=>	'PermisosController@perEspecialMarino',
		'as'	=>	'permisos.marinosFiltro'
	]);

	Route::post('Especial/pdf/marino',[
		'uses'	=>	'PermisosController@pdfPerEspecialMarino',
		'as'	=>	'permisos.marinos.pdf'
	]);

});
